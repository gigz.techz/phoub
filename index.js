/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App'; // Check the path to App component
import {name as appName} from './app.json';
import {Provider} from 'react-redux';
import store from './src/Service/redux/Store/store';

const ReduxApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => ReduxApp);
