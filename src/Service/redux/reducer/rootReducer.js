import {combineReducers} from 'redux';
import dataReducer from './dataSlice'; // Import the combined reducer

export default combineReducers({
  data: dataReducer, // Use the combined reducer here
});