import {createSlice} from '@reduxjs/toolkit';

export const dataSlice = createSlice({
  name: 'data',
  initialState: {
    hashTag: [],
    mentionPeople: [],
    postImages: [],
    user: {},
    freelancerService: {},
    studioService: {},
  },
  reducers: {
    saveHashTag: (state, action) => {
      state.hashTag = [...state.hashTag, action.payload];
    },
    saveMentionPeople: (state, action) => {
      state.mentionPeople = [...state.mentionPeople, action.payload];
    },
    savePostImages: (state, action) => {
      state.postImages = action.payload;
    },
    saveUser: (state, action) => {
      state.user = action.payload;
    },
    saveFreelancerService: (state, action) => {
      state.freelancerService = action.payload;
    },
    saveStudioService: (state, action) => {
      state.studioService = action.payload;
    },
  },
});

export const {
  saveHashTag,
  saveMentionPeople,
  savePostImages,
  saveUser,
  saveFreelancerService,
  saveStudioService,
} = dataSlice.actions;

export default dataSlice.reducer;
