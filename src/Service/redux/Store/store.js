import { configureStore } from '@reduxjs/toolkit';
import dataReducer from '../reducer/dataSlice'; // Assuming your dataSlice export is named dataReducer

const store = configureStore({
  reducer: {
    data: dataReducer, // Use dataReducer with the key 'data'
    // ...other reducers if present
  },
});

export default store;