const BASE_URL = 'http://13.51.200.31:8080/';
// const BASE_URL = 'http://192.168.1.18:8080/';

import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const studioRegistration = async payload => {
  let ApiResponse = {};

  console.log('PAYLOAD', payload);
  const data = {
    profileImage: payload.profileImage ? payload.profileImage : null,
    fullName: payload.fullName,
    address: payload.address,
    city: payload.city,
    username: payload.username,
    password: payload.password,
    mobileNumber: payload.mobileNumber,
    role: payload.role,
    code: payload.code,
    studioName: payload.studioName ? payload.studioName : null,
    ownerName: payload.ownerName ? payload.ownerName : null,
    governmentRegistrationId: payload.govId ? payload.govId : null,
    serviceList: payload.serviceList ? payload.serviceList : null,
  };

  try {
    var config = {
      method: 'post',
      url: BASE_URL + 'api/public/signup',
      data: data,
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'Studio Registration => ');
        ApiResponse = JSON.stringify(response.data);
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

// export const customerRegistration = async payload => {
//   let ApiResponse = {};

//   const data = {
//     cfullname: payload.fullName,
//     caddress: payload.address,
//     ccity: payload.city,
//     cemail: payload.email,
//     cpassword: parseInt(payload.password),
//   };
//   console.log(data);

//   try {
//     var config = {
//       method: 'post',
//       url: BASE_URL + 'phoub/auth/register/customer',
//       data: data,
//     };

//     return axios(config)
//       .then(function (response) {
//         console.log(response.data, 'Customer Registration => ');
//         ApiResponse = response;
//         return ApiResponse;
//       })
//       .catch(function (error) {
//         console.log('axios', error);
//         return null;
//       });
//   } catch (e) {
//     console.log('Network', e);
//   }
// };

// export const freelancerRegistration = async payload => {
//   let ApiResponse = {};

//   const data = {
//     ffullname: payload.fullName,
//     faddress: payload.address,
//     fcity: payload.city,
//     fcode: payload.code,
//     fphoneNumber: payload.mobile,
//     femail: payload.email,
//     fpassword: parseInt(payload.password),
//   };

//   try {
//     var config = {
//       method: 'post',
//       url: BASE_URL + 'phoub/auth/register/freelancer',
//       data: data,
//     };

//     return axios(config)
//       .then(function (response) {
//         console.log(
//           JSON.stringify(response.data),
//           'Freelancer Registration => ',
//         );
//         ApiResponse = JSON.stringify(response.data);
//         return ApiResponse;
//       })
//       .catch(function (error) {
//         console.log(error);
//         return null;
//       });
//   } catch (e) {
//     console.log(e);
//   }
// };

export const studioLogin = async payload => {
  console.log('PAyload', payload);
  try {
    const data = {
      username: payload.username,
      password: payload.password,
    };

    const config = {
      method: 'post',
      url: BASE_URL + 'api/public/login',
      data: data,
    };

    const response = await axios(config);
    console.log(JSON.stringify(response.data), 'Studio Login => ');

    const accessToken = response.data.token;
    console.log('AccessToken', accessToken);

    AsyncStorage.setItem('BearerToken', accessToken);
    AsyncStorage.setItem('username', payload.username);
    AsyncStorage.setItem('password', payload.password);

    return response.data;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const changePassword = async payload => {
  const token = await AsyncStorage.getItem('BearerToken');

  console.log('PAyload', payload);
  try {
    const data = {
      currentPassword: payload.currentPassword,
      newPassword: payload.password,
    };

    const config = {
      method: 'put',
      url: BASE_URL + 'api/protected/change-password',
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const response = await axios(config);
    console.log(JSON.stringify(response.data), 'Password change => ');

    return response.data;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const photoPost = async payload => {
  let ApiResponse = {};
  console.log('PAYLOAD', payload);
  const data = {
    content: payload.content,
    tags: payload.tags,
    mentions: payload.mentions,
    path: payload.mediaList,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + 'api/protected/post/create',
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'Photo post => ');
        ApiResponse = JSON.stringify(response.data);
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getImagePath = async payload => {
  console.log('PAYLOAD DATA', payload);
  const data = payload;
  try {
    const formData = new FormData();
    data &&
      data.map(item =>
        formData.append('file', {
          uri: item.uri,
          name: 'image.jpg',
          type: 'image/jpeg',
        }),
      );

    console.log('MULTIPART', formData);

    const config = {
      method: 'post',
      url: BASE_URL + 'api/public/file/upload',
      data: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    };

    const response = await axios(config);
    console.log(JSON.stringify(response.data), 'IMAGE RESPONSE => ');

    return response.data;
  } catch (error) {
    console.log(error);
    return null;
  }
};

export const addTask = async payload => {
  let ApiResponse = {};
  // console.log('PAYLOAD', payload);
  const data = {
    title: payload.title,
    dueDate: payload.dueDate,
    description: payload.description,
    participantIds: payload.participantIds,
  };
  console.log('PAYLOAD', data);
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + 'api/protected/schedule/task/create',
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'Photo post => ');
        ApiResponse = JSON.stringify(response.data);
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getAllTask = async () => {
  let ApiResponse = {};
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + 'api/protected/schedule/task-fetch?page=0&size=10',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'ALL tAsK => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const searchProfile = async payload => {
  let ApiResponse = {};
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + 'api/protected/search/profile?query=' + payload,
      data: ApiResponse,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'Search Profile => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getAllPost = async payload => {
  let ApiResponse = {};
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + 'api/protected/post/feed',
      data: ApiResponse,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'All Post => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const postLikeDislike = async payload => {
  let ApiResponse = {};
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + `api/protected/post/${payload}/like-dislike`,
      data: ApiResponse,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'All Post => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getAllComment = async payload => {
  let ApiResponse = {};
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + `api/protected/post/comment-fetch?postId=${payload}`,
      data: ApiResponse,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'all comment => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const postComment = async payload => {
  let ApiResponse = {};
  const data = {
    commentContent: payload.commentContent,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url:
        BASE_URL + `api/protected/post/create/comment?postId=${payload.postId}`,
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'comment => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const deleteComment = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'delete',
      url: BASE_URL + `api/protected/post/comments/${payload}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'dlete comment => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const editComment = async payload => {
  let ApiResponse = {};
  const data = {
    commentContent: payload.commentContent,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'put',
      data: data,
      url: BASE_URL + `api/protected/post/comments/${payload.commentId}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'edit comment => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const replyOnComment = async payload => {
  let ApiResponse = {};
  const data = {
    commentContent: payload.commentContent,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url:
        BASE_URL +
        `api/protected/post/${payload.commentId}/comments/${payload.postId}/reply`,
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'reply comment => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const deletePost = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'delete',
      url: BASE_URL + `api/protected/post/delete/${payload.postId}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'delete post => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getProfile = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + `api/protected/profile${payload ? '/' + payload : ''}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'profile => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getTaskByDate = async payload => {
  let ApiResponse = {};
  console.log('DATE PAUYYMLOAD', payload);
  const data = {
    month: parseInt(payload.month),
    year: parseInt(payload.year),
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + `api/protected/schedule/tasks?page=0&size=10`,
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'profile => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const createGroup = async payload => {
  let ApiResponse = {};
  console.log('GROPSSS', payload);
  const data = {
    groupName: payload.groupName,
    groupImage: null,
    groupDescription: '',
    participantIds: payload.invited,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + `api/protected/v1/chat/create-group`,
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'GROP Created => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const fetchALLGroup = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + 'api/protected/v1/chat/get-groups',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'GROP ALL => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const allGroupMessage = async payload => {
  let ApiResponse = {};
  const data = {
    groupId: payload,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + 'api/protected/v1/chat/receive-group-message',
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'GROP ALL message => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const sendGroupMessage = async payload => {
  let ApiResponse = {};
  const data = {
    groupId: payload.groupId,
    message: payload.message,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      url: BASE_URL + 'api/protected/v1/chat/send-group-message',
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'message => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getUserRole = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + 'api/protected/role-and-id',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'Role => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const fetchALLChat = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + 'api/protected/v1/chat/get-all-chat',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'ALl chat => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const sendSingleChat = async payload => {
  let ApiResponse = {};
  const data = {
    message: payload.message,
    receiverUserId: payload.userId,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      data: data,
      url: BASE_URL + 'api/protected/v1/chat/send-message',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'ALl chat => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const fetchSingleChat = async payload => {
  let ApiResponse = {};
  const data = {
    messageSenderId: payload.userId,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      data: data,
      url: BASE_URL + 'api/protected/v1/chat/get-message',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'ALl chat => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getNotification = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + 'api/protected/notification/push-notify',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'Notification => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const editProfile = async payload => {
  let ApiResponse = {};
  const data = {
    fullName: payload.fullName,
    address: payload.address,
    profilePicturePath: payload.profilePicturePath,
    mobileNumber: payload.mobileNumber,
    studioName: payload.studioName,
    ownerName: payload.ownerName,
    governmentRegistrationId: payload.governmentRegistrationId,
    description: payload.description,
    socialMediaLinks: payload.socialMediaLinks,
  };
  console.log('PAYLOAD DATA', data);
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'put',
      data: data,
      url: BASE_URL + 'api/protected/profile',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'EDIT => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const seenNotification = async payload => {
  let ApiResponse = {};
  console.log('NOTIFICATIO', payload);
  const data = {
    notificationIds: [payload],
  };
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'put',
      data: data,
      url: BASE_URL + 'api/protected/notification/push-notify',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'EDIT => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const addPhoubPhoto = async payload => {
  let ApiResponse = {};
  const data = {
    photosPath: payload,
  };
  const token = await AsyncStorage.getItem('BearerToken');
  console.log('PAYLOAD', data);
  try {
    var config = {
      method: 'post',
      data: data,
      url: BASE_URL + 'api/protected/photo-uploads',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'ADDED => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const getPhoubPhoto = async payload => {
  let ApiResponse = {};

  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'get',
      url: BASE_URL + 'api/protected/photo-uploads',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'get Phoub => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};

export const sharePhotoBook = async payload => {
  let ApiResponse = {};
  const data = {
    userId: payload,
  };
  console.log('PAYLOAD', data);
  const token = await AsyncStorage.getItem('BearerToken');
  try {
    var config = {
      method: 'post',
      data: data,
      url: BASE_URL + 'api/protected/share/media',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    return axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data), 'share Phoub => ');
        ApiResponse = response.data;
        return ApiResponse;
      })
      .catch(function (error) {
        console.log(error);
        return null;
      });
  } catch (e) {
    console.log(e);
  }
};
