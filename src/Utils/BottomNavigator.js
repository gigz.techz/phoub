import {View, Text, Image, StyleSheet} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../container/Home/Home';
import Search from '../container/Search/Search';
import Account from '../container/Account/Account';
import ExploreScreen from '../container/Explore/ExploreScreen';
import ChatScreen from '../container/Chats/ChatScreen';
import Icon from 'react-native-vector-icons/Foundation';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Octicons from 'react-native-vector-icons/Octicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const Bottom = createBottomTabNavigator();
const BottomNavigator = () => {
  return (
    <Bottom.Navigator>
      <Bottom.Screen
        name="Home"
        component={Home}
        options={{
          headerShown: false,
          tabBarLabel: () => false,
          tabBarIcon: tabInfo => {
            return (
              <View style={tabInfo.focused ? styles.bottomTab : ''}>
                {tabInfo.focused ? (
                  <Icon color={'#183761'} name="home" size={25} />
                ) : (
                  <Octicons color={'#7487A0'} name="home" size={22} />
                )}
              </View>
            );
          },
          tabBarLabelStyle: {
            fontSize: 14,
          },
        }}
      />
      <Bottom.Screen
        name="Search"
        component={Search}
        options={{
          headerShown: false,
          tabBarLabel: () => false,
          tabBarIcon: tabInfo => {
            return (
              <View style={tabInfo.focused ? styles.bottomTab : ''}>
                <Fontisto
                  color={tabInfo.focused ? '#183761' : '#7487A0'}
                  name="search"
                  size={22}
                />
              </View>
            );
          },
          tabBarLabelStyle: {
            fontSize: 14,
          },
        }}
      />
      <Bottom.Screen
        name="Explore"
        component={ExploreScreen}
        options={{
          headerShown: false,
          tabBarLabel: () => false,
          tabBarIcon: tabInfo => {
            return (
              <Image
                source={require('../assets/images/explore.png')}
                style={{
                  width: 40,
                  height: 40,
                }}
              />
            );
          },
          tabBarLabelStyle: {
            fontSize: 14,
          },
        }}
      />
      <Bottom.Screen
        name="Chats"
        component={ChatScreen}
        options={{
          headerShown: false,
          tabBarLabel: () => false,
          tabBarIcon: tabInfo => {
            return (
              <View style={tabInfo.focused ? styles.bottomTab : ''}>
                <Image
                  source={require('../assets/images/chat.png')}
                  style={{
                    width: 25,
                    height: 25,
                    tintColor: tabInfo.focused ? '#183761' : '#7487A0',
                  }}
                />
              </View>
            );
          },
          tabBarLabelStyle: {
            fontSize: 14,
          },
        }}
      />
      <Bottom.Screen
        name="Account"
        component={Account}
        options={{
          headerShown: false,
          tabBarLabel: () => false,
          tabBarIcon: tabInfo => {
            return (
              <FontAwesome5
                name="user-alt"
                size={25}
                color={tabInfo.focused ? '#183761' : '#7487A0'}
              />
            );
          },
          tabBarLabelStyle: {
            fontSize: 14,
          },
        }}
      />
    </Bottom.Navigator>
  );
};
const styles = StyleSheet.create({
  bottomTab: {
    borderRadius: 20,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#D8DFE3',
    height: '100%',
  },
});

export default BottomNavigator;
