import Socket from 'socket.io-client';

const SOCKET_URL = 'http://192.168.1.18:8082/ws';

const socketServices = Socket.connect(SOCKET_URL);
const chatMessage = {};
const chatMessages = {};

export default socketServices;

// var socketServices = new SockJS(SOCKET_URL);
// const socketServices = Stomp.client(SOCKET_URL);
// class WSService {
//   initializeSocket = async () => {
//     try {
//       this.socket = io(SOCKET_URL, {
//         transports: ['websocket'],
//       });
//       console.log('initializing socket', this.socket);

//       this.socket.on('connect', data => {
//         console.log('socket connected');
//       });
//       this.socket.on('disconnect', data => {
//         console.log('socket disconnected');
//       });
//       this.socket.on('error', data => {
//         console.log('socket error');
//       });
//     } catch (error) {
//       console.log('socket is not initialized', error);
//     }
//   };

//   emit(event, data = {}) {
//     this.socket.emit(event, data);
//   }
//   on(event, cb) {
//     this.socket.on(event, cb);
//   }

//   removeListener(listenerName) {
//     this.socket.removeListener(listenerName);
//   }
// }

// const socketServices = new WSService()
