import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
// import Parent from "./Parent";
import SplashScreen from '../container/WelcomeScreen/SplashScreen';
import WelcomeScreen from '../container/WelcomeScreen/WelcomeScreen';
import LoginScreen from '../container/LoginScreen/LoginScreen';
import Classification from '../container/Classification/Classification';
import FreelancerPersonalDetail from '../container/Freelancer/FreelancerPersonalDetail';
import FreelancerEmailDetail from '../container/Freelancer/FreelancerEmailDetail';
import SuccessScreen from '../container/SuccessScreen/SuccessScreen';
import StudioPersonalDetail from '../container/Studio/StudioPersonalDetail';
import StudioEmailDetail from '../container/Studio/StudioEmailDetail';
import CustomerPersonalDetail from '../container/Customer/CustomerPersonalDetail';
import EmailVerification from '../container/Customer/EmailVerification';
import CreatePassword from '../container/Customer/CreatePassword';
import Parent from './Parent';
import AllTaskScreen from '../container/Home/AllTaskScreen';
import GroupChatBox from '../container/Chats/GroupChatBox';
import WelcomeSuccess from '../container/SuccessScreen/WelcomeSuccess';
import SettingScreen from '../container/Setting/SettingScreen';
import PostDescriptionScreen from '../container/Explore/PostDescriptionScreen';
import AddHashTagScreen from '../container/Explore/AddHashTagScreen';
import TagAccountScreen from '../container/Explore/TagAccountScreen';
import CalendarScreen from '../container/Task/CalendarScreen';
import AddTaskScreen from '../container/Task/AddTaskScreen';
import ServiceFreelancer from '../container/Freelancer/ServiceFreelancer';
import CreateGroupChat from '../container/Chats/CreateGroupChat';
import EditProfile from '../container/Account/EditProfile';
import SingleChatBox from '../container/Chats/SIngleChatBox';
import Notification from '../container/Home/Notification';
import NotesPage from '../container/Home/NotesPage';
import AddPhotos from '../container/Account/AddPhotos';
import ShareBook from '../container/Account/ShareBook';
import SearchAccount from '../container/Search/SearchAccount';
import ServiceStudio from '../container/Studio/ServiceStudio';

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="WelcomeScreen"
          component={WelcomeScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Classification"
          component={Classification}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="FreelancerPersonalDetail"
          component={FreelancerPersonalDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="FreelancerEmailDetail"
          component={FreelancerEmailDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="StudioPersonalDetail"
          component={StudioPersonalDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="StudioEmailDetail"
          component={StudioEmailDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CustomerPersonalDetail"
          component={CustomerPersonalDetail}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EmailVerification"
          component={EmailVerification}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SuccessScreen"
          component={SuccessScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CreatePassword"
          component={CreatePassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Parent"
          component={Parent}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AllTaskScreen"
          component={AllTaskScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="GroupChatBox"
          component={GroupChatBox}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SingleChatBox"
          component={SingleChatBox}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="WelcomeSuccess"
          component={WelcomeSuccess}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SettingScreen"
          component={SettingScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="PostDescriptionScreen"
          component={PostDescriptionScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AddHashTagScreen"
          component={AddHashTagScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="TagAccountScreen"
          component={TagAccountScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CalendarScreen"
          component={CalendarScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AddTaskScreen"
          component={AddTaskScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ServiceFreelancer"
          component={ServiceFreelancer}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ServiceStudio"
          component={ServiceStudio}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CreateGroupChat"
          component={CreateGroupChat}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="EditProfile"
          component={EditProfile}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="NotificationScreen"
          component={Notification}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="NotesPage"
          component={NotesPage}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="AddPhotos"
          component={AddPhotos}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ShareBook"
          component={ShareBook}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="SearchAccount"
          component={SearchAccount}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
