import {View, Text, Image, TouchableOpacity} from 'react-native';
import React, {useRef, useState} from 'react';
import ReadMore from 'react-native-read-more-text';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {getAllComment, postLikeDislike} from '../Service/Api';

const FeedCard = ({item, getAllPost, handleShow, handleAllComment}) => {
  const isCarousel = useRef(null);
  const [index, setIndex] = useState(0);

  const renderItem = ({item}) => (
    <View
      style={{
        marginTop: 20,
        alignItems: 'center',
        borderRadius: 10,
      }}>
      <Image source={{uri: item.path}} style={{height: 200, width: '100%'}} />
    </View>
  );

  const handleLike = async () => {
    const response = await postLikeDislike(item.postId);
    console.log('LIKE', response);
    getAllPost();
  };

  const handleComment = async () => {
    handleShow(item.postId);
    handleAllComment(item.postId);
  };

  return (
    <View style={{padding: 10}}>
      <View style={{flex: 0, flexDirection: 'row'}}>
        <View style={{height: 32, width: 32, justifyContent: 'center'}}>
          <Image
            style={{height: '100%', width: '100%', borderRadius: 100}}
            source={require('../assets/images/test1.png')}
          />
        </View>
        <View style={{marginLeft: 10}}>
          <View style={{flex: 0, flexDirection: 'row'}}>
            <Text style={{color: '#183761', fontSize: 14, fontWeight: '700'}}>
              {item.postCreatedBy}
            </Text>
            <Text
              style={{
                color: '#A3AFC0',
                fontSize: 14,
                fontWeight: '400',
                marginLeft: 5,
              }}>
              has tagged
            </Text>
            <Text
              style={{
                color: '#183761',
                fontSize: 14,
                fontWeight: '700',
                marginLeft: 5,
              }}>
              Outlook Studio
            </Text>
          </View>
          <View>
            <Text style={{color: '#D1D7DF', fontSize: 10, fontWeight: '500'}}>
              10 days ago
            </Text>
          </View>
        </View>
      </View>
      <View style={{marginTop: 10}}>
        <ReadMore
          numberOfLines={2}
          renderTruncatedFooter={handlePress => (
            <Text
              onPress={handlePress}
              style={{color: '#183761', fontWeight: '700', marginTop: 5}}>
              (more)
            </Text>
          )}
          renderRevealedFooter={handlePress => (
            <Text
              onPress={handlePress}
              style={{color: '#183761', fontWeight: '700', marginTop: 5}}>
              (less)
            </Text>
          )}>
          <Text style={{color: '#2E2E2E', fontWeight: '400', fontSize: 12}}>
            {item.content}
            {'\n\n'}
            <View style={{flex: 0, flexDirection: 'row', marginTop: 20}}>
              {item.tags &&
                item.tags.map(items => (
                  <View
                    style={{
                      padding: 10,
                      borderRadius: 15,
                      backgroundColor: '#465F81',
                      margin: 5,
                      minWidth: 50
                    }}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontSize: 11,
                        fontWeight: '400',
                        textAlign: "center"
                      }}>
                      {items}
                    </Text>
                  </View>
                ))}
            </View>
          </Text>
        </ReadMore>
      </View>
      <View style={{marginTop: 10}}>
        <Carousel
          ref={isCarousel}
          data={item.mediaList}
          renderItem={renderItem}
          sliderWidth={330}
          itemWidth={330}
          onSnapToItem={currentIndex => setIndex(currentIndex)}
        />
        <View style={{marginTop: -20}}>
          <Pagination
            dotsLength={item.mediaList.length}
            activeDotIndex={index}
            carouselRef={isCarousel}
            dotStyle={{
              width: 15,
              height: 10,
              borderRadius: 10,
              backgroundColor: 'rgb(116, 135, 160)',
            }}
          />
        </View>
      </View>
      <View style={{padding: 10, paddingLeft: 20}}>
        <View style={{flex: 0, flexDirection: 'row'}}>
          <Text style={{fontSize: 10, fontWeight: '700', color: '#B1BFC7'}}>
            Liked by
          </Text>
          <Text
            style={{
              fontSize: 10,
              fontWeight: '700',
              color: '#3C6074',
              marginLeft: 5,
            }}>
            Outlook Studios and 100 others
          </Text>
        </View>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            marginTop: 5,
            marginBottom: 10,
          }}>
          <TouchableOpacity onPress={() => handleLike()}>
            {item.likedOrNot ? (
              <FontAwesome size={23} color="#465F81" name="heart" />
            ) : (
              <FontAwesome size={23} color="#465F81" name="heart-o" />
            )}
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '400',
              color: '#465F81',
              alignSelf: 'center',
              marginLeft: 5,
            }}>
            {item.likeCount}
          </Text>
          <TouchableOpacity onPress={() => handleComment()}>
            <MaterialIcons
              size={23}
              color="#465F81"
              name="chat-bubble-outline"
              style={{marginLeft: 20}}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '400',
              color: '#465F81',
              marginLeft: 5,
              alignSelf: 'center',
            }}>
            {item.commentCount}
          </Text>
        </View>
        {/* <View>
          <TouchableOpacity
            onPress={() => {
              handleAllComment(item.postId);
              handleShow(item.postId);
            }}>
            <Text
              style={{
                fontSize: 14,
                fontWeight: '600',
                color: '#465F81',
                marginTop: 10,
              }}>
              View All Comments
            </Text>
          </TouchableOpacity>
        </View> */}
      </View>
    </View>
  );
};

export default FeedCard;
