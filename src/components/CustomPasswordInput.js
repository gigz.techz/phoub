import {View, Text, TextInput, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';

const CustomPasswordInput = ({placeholder, onChange}) => {
  const [isPasswordSecure, setIsPasswordSecure] = useState(true);
  return (
    <View style={styles.input}>
      <TextInput
        onChangeText={onChange}
        style={{color: '#FFFFFF', width: '90%'}}
        placeholder={placeholder}
        placeholderTextColor="#FFFFFF"
        secureTextEntry={isPasswordSecure}></TextInput>
      <Icon
        style={{alignSelf: 'center'}}
        name={isPasswordSecure ? 'eye-slash' : 'eye'}
        size={20}
        color="#FFFFFF"
        onPress={() => {
          isPasswordSecure
            ? setIsPasswordSecure(false)
            : setIsPasswordSecure(true);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 8,
    borderWidth: 1,
    backgroundColor: 'rgba(52, 52, 52, 0.7)',
    padding: 5,
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '400',
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default CustomPasswordInput;
