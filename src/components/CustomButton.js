import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React from "react";

const CustomButton = ({ text, disabled, onPress }) => {
  return (
    <View>
      <TouchableOpacity disabled={disabled ? disabled : false} onPress={onPress}>
        <View style={styles.signUp}>
          <Text
            style={{
              textAlign: "center",
              fontSize: 20,
              fontWeight: "600",
              color: "#183761",
            }}
          >
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  signUp: {
    backgroundColor: "#FFFFFF",
    padding: 20,
    width: "100%",
    borderRadius: 8,
  },
});

export default CustomButton;
