import { View, Text, TextInput, StyleSheet } from "react-native";
import React from "react";

const CustomInput = ({ placeholder, password, onChange }) => {
  return (
    <View>
      <TextInput
        onChangeText={onChange}
        secureTextEntry={password ? password : false}
        style={styles.input}
        placeholder={placeholder}
        placeholderTextColor="#FFFFFF"
      ></TextInput>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 8,
    borderWidth: 1,
    backgroundColor: "rgba(52, 52, 52, 0.7)",
    padding: 15,
    borderColor: "#FFFFFF",
    color: "#FFFFFF",
    fontSize: 16,
    fontWeight: "400",
  },
});

export default CustomInput;
