import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome6';

const CustomServiceInput = ({placeholder, onChange, onPress}) => {
  return (
    <View style={styles.input}>
      <TextInput
        onChangeText={onChange}
        style={{color: '#FFFFFF', width: '90%'}}
        placeholder={placeholder}
        placeholderTextColor="#FFFFFF"></TextInput>
      <View style={{justifyContent: 'center'}}>
        <TouchableOpacity onPress={onPress}>
          <Icon
            style={{alignSelf: 'center'}}
            name="circle-arrow-down"
            size={20}
            color="#FFFFFF"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 8,
    borderWidth: 1,
    backgroundColor: 'rgba(52, 52, 52, 0.7)',
    padding: 5,
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '400',
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default CustomServiceInput;
