import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

const CustomHeader = ({onPress, bell}) => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
      }}>
      <View style={{justifyContent: 'center'}}>
        <Image source={require('../assets/images/head-logo.png')} />
      </View>
      <TouchableOpacity onPress={onPress}>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            padding: 10,
            backgroundColor: '#D8DFE3',
            borderRadius: 40,
            paddingLeft: 10,
            paddingRight: 10,
          }}>
          <Text>Notification</Text>
          <Icon
            name="notifications"
            size={20}
            color={bell ? 'orange' : '#183761'}
            style={{marginLeft: 10}}
          />
        </View>
      </TouchableOpacity>
      <View style={{justifyContent: 'center'}}>
        <TouchableOpacity onPress={() => navigation.navigate('SettingScreen')}>
          <Icon name="settings-sharp" size={21} color="#3C6074" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CustomHeader;
