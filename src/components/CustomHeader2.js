import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import AntDesign from "react-native-vector-icons/AntDesign";

const CustomHeader2 = ({ text, navigation }) => {
  return (
    <View
      style={{ flex: 0, flexDirection: "row", justifyContent: "space-between" }}
    >
      <View>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="arrowleft" size={24} color='#183761' />
        </TouchableOpacity>
      </View>
      <View>
        <Text style={{ fontSize: 16, fontWeight: "500", color: "#0C1317" }}>
          {text}
        </Text>
      </View>
      <View>{}</View>
    </View>
  );
};

export default CustomHeader2;
