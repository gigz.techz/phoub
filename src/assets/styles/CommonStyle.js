import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  LOADING_WRAPPER: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  LOADING_BOX: {
    backgroundColor: '#FFFFFF',
    borderRadius: 100,
    height: 20,
    width: 20,
  },
  LOADING_BOX_PENDING: {
    backgroundColor: '#0E213A',
    borderRadius: 100,
    height: 20,
    width: 20,
  },
  LOADING_LABEL: {
    fontWeight: 'bold',
    fontSize: 30,
    color: '#393ac1',
    marginTop: 30,
    marginBottom: 100,
  },
  LOADING_LINE: {
    backgroundColor: '#FFFFFF',
    height: 3,
    width: 40,
    alignSelf: 'center',
  },
  LOADING_LINE_PENDING: {
    backgroundColor: '#0E213A',
    height: 3,
    width: 40,
    alignSelf: 'center',
  },

  PASSWORD_WRAPPER: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: 5,
    width: '100%',
    backgroundColor: '#233348',
    borderRadius: 16,
  },
  PASSWORD1: {
    width: '2%',
    backgroundColor: 'red',
    borderRadius: 16,
  },
  PASSWORD_LABEL1: {
    width: '98%',
    backgroundColor: '#233348',
    borderRadius: 16,
  },
  PASSWORD4: {
    width: '20%',
    backgroundColor: 'red',
    borderRadius: 16,
  },
  PASSWORD_LABEL4: {
    width: '80%',
    backgroundColor: '#233348',
    borderRadius: 16,
  },
  PASSWORD6: {
    width: '60%',
    backgroundColor: 'orange',
    borderRadius: 16,
  },
  PASSWORD_LABEL6: {
    width: '40%',
    backgroundColor: '#233348',
    borderRadius: 16,
  },
  PASSWORD8: {
    width: '75%',
    backgroundColor: 'green',
    borderRadius: 16,
  },
  PASSWORD_LABEL8: {
    width: '25%',
    backgroundColor: '#233348',
    borderRadius: 16,
  },
  PASSWORD10: {
    width: '100%',
    backgroundColor: 'green',
    borderRadius: 16,
  },
});
