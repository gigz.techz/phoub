import {View, Text} from 'react-native';
import React, {useEffect} from 'react';

const WelcomeSuccess = ({navigation}) => {
    useEffect(() => {
      setTimeout(() => {
        navigation.navigate('Parent');
      }, 3000);
    }, []);
  return (
    <View
      style={{
        flex: 0,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        backgroundColor: '#183761',
      }}>
      <View style={{paddingLeft: 30, paddingRight: 30}}>
        <Text style={{fontSize: 20, color: '#FEFEFE', fontWeight: '400', textAlign: "center"}}>
          Welcome to Phoub, your portal to a vibrant community of passion.
        </Text>
      </View>
    </View>
  );
};

export default WelcomeSuccess;
