import {View, Text} from 'react-native';
import React from 'react';
import {ImageBackground} from 'react-native';
import {StyleSheet} from 'react-native';
import {Image} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CommonStyle from '../../assets/styles/CommonStyle';

const SuccessScreen = ({navigation}) => {
  return (
    <View>
      <View style={styles.main}>
        <View style={{marginTop: 30}}>
          <View style={CommonStyle.LOADING_WRAPPER}>
            <View style={CommonStyle.LOADING_BOX} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_BOX} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_BOX} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_BOX} />
          </View>
        </View>
        <View style={{marginTop: 100}}>
          <View>
            <Text style={styles.head}>Success!</Text>
          </View>
          <View>
            <Text style={{fontSize: 16, fontWeight: '400', color: '#FFFFFF'}}>
              Your account has been created.
            </Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <Image source={require('../../assets/images/success.png')} />
          </View>
          <View>
            <CustomButton
              onPress={() => navigation.navigate('WelcomeSuccess')}
              text="Continue to Phoub"
            />
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 50,
    fontWeight: '600',
    lineHeight: 65,
  },
  backGroundImage: {
    height: '100%',
    width: '100%',
  },
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: '#183761',
    padding: 30,
  },
});

export default SuccessScreen;
