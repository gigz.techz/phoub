import {View, Text, TextInput, StyleSheet, Image, Modal} from 'react-native';
import React, {useState} from 'react';
import SearchCard from './SearchCard';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {TouchableOpacity} from 'react-native';
import FreelancerFilter from './FreelancerFilter';
import StudioFilter from './StudioFilter';
import {searchProfile} from '../../Service/Api';
import {useSelector} from 'react-redux';

const Search = () => {
  const [highlight, setHighlight] = useState(true);
  const [show, setShow] = useState(false);
  const [searchItem, setSearchItem] = useState('');
  const [searchData, setSearchData] = useState({});
  const {user} = useSelector(state => state.data);
  const role = user.message;

  const handleSearch = async () => {
    const response = await searchProfile(searchItem);
    console.log('SEARCH', response.content);
    setSearchData(response);
  };

  return (
    <View onBlur={() => setHighlight(true)} style={{padding: 10}}>
      <View>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            onFocus={() => setHighlight(false)}
            style={[
              styles.searchContainer,
              {borderColor: highlight ? '#D1D7DF' : '#183761'},
            ]}>
            <TextInput
              style={styles.input}
              placeholder="Search..."
              onChangeText={setSearchItem}
              value={searchItem}
            />
            <TouchableOpacity onPress={() => handleSearch()}>
              <Image
                source={require('../../assets/images/search.png')}
                style={[
                  styles.icon,
                  {tintColor: highlight ? '#D1D7DF' : '#183761'},
                ]}
              />
            </TouchableOpacity>
          </View>
          <View style={{justifyContent: 'center'}}>
            <TouchableOpacity onPress={() => setShow(!show)}>
              <Ionicons name="filter" size={24} color="#183761" />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 10,
            marginTop: 10,
          }}>
          <View>
            <Text style={{fontSize: 16, fontWeight: '400', color: '#A3AFC0'}}>
              Recently viewed
            </Text>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => {
                setSearchData('');
                setSearchItem('');
              }}>
              <Text style={{fontSize: 16, fontWeight: '600', color: '#183761'}}>
                Clear all
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View>
          {searchData &&
            searchData.content &&
            searchData.content.map((item, i) => (
              <View>
                <SearchCard name={item.name} id={item.id} />
              </View>
            ))}
        </View>
      </View>
      <View>
        <Modal
          style={{height: 200}}
          transparent={true}
          animationType="slide"
          visible={show}
          onRequestClose={() => {
            setShow(!show);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              {role == 'FREELANCER' && <FreelancerFilter />}
              {role == 'STUDIO' && <StudioFilter />}
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 40,
    height: 40,
    paddingHorizontal: 10,
    width: '85%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
    color: 'black',
  },
  flexBetween: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dateInput: {
    borderColor: '#9C9C9C',
    borderWidth: 1,
    borderRadius: 8,
    fontSize: 16,
    fontWeight: '400',
    padding: 10,
    paddingLeft: 35,
    paddingRight: 35,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});

export default Search;
