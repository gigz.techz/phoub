import {View, Text, StyleSheet, TextInput} from 'react-native';
import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';

const StudioFilter = () => {
  return (
    <View>
      <View style={styles.flexBetween}>
        <View>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#2E2E2E',
            }}>
            Filter search
          </Text>
        </View>
        <View>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: '#183761',
            }}>
            Apply filter
          </Text>
        </View>
      </View>
      <View>
        <Text
          style={{
            fontSize: 16,
            fontWeight: '400',
            color: '#A3AFC0',
            marginTop: 15,
          }}>
          Search for
        </Text>
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          marginTop: 10,
          gap: 10,
        }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Studio
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Freelancer
          </Text>
        </View>
      </View>
      <View>
        <Text
          style={{
            fontSize: 16,
            fontWeight: '400',
            color: '#A3AFC0',
            marginTop: 15,
          }}>
          List of services
        </Text>
      </View>
      <View style={{marginTop: 10}}>
        <TextInput
          style={[styles.dateInput, {paddingLeft: 10, paddingRight: 10}]}
          placeholder="Type of service"
          placeholderTextColor="#9C9C9C"
        />
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          marginTop: 10,
          gap: 10,
        }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Housing Ceremony
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Birthday Ceremony
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Marraige Ceremony
          </Text>
        </View>
      </View>
      <View>
        <Text
          style={{
            fontSize: 16,
            fontWeight: '400',
            color: '#A3AFC0',
            marginTop: 15,
          }}>
          By Location
        </Text>
      </View>
      <View style={{marginTop: 10}}>
        <TextInput
          style={[styles.dateInput, {paddingLeft: 10, paddingRight: 10}]}
          placeholder="Enter a location"
          placeholderTextColor="#9C9C9C"
        />
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          marginTop: 10,
          gap: 10,
        }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Pune
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Bangalore
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Delhi
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Kolkata
          </Text>
        </View>
      </View>
      <View>
        <Text
          style={{
            fontSize: 16,
            fontWeight: '400',
            color: '#A3AFC0',
            marginTop: 15,
          }}>
          By date
        </Text>
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 10,
        }}>
        <View>
          <TextInput
            style={styles.dateInput}
            placeholder="dd"
            placeholderTextColor="#9C9C9C"
          />
        </View>
        <View style={{justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: '#9C9C9C',
            }}>
            -
          </Text>
        </View>
        <View>
          <TextInput
            style={styles.dateInput}
            placeholder="mm"
            placeholderTextColor="#9C9C9C"
          />
        </View>
        <View style={{justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: '600',
              color: '#9C9C9C',
            }}>
            -
          </Text>
        </View>
        <View>
          <TextInput
            style={styles.dateInput}
            placeholder="yyyy"
            placeholderTextColor="#9C9C9C"
          />
        </View>
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          marginTop: 10,
          gap: 10,
        }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Today
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            Tomorrow
          </Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#183761',
            padding: 10,
            borderRadius: 20,
            flex: 0,
            flexDirection: 'row',
            gap: 8,
          }}>
          <AntDesign name="pluscircle" size={16} color="#183761" />
          <Text
            style={{
              fontSize: 10,
              fontWeight: '600',
              color: '#183761',
            }}>
            After Tomorrow
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 40,
    height: 40,
    paddingHorizontal: 10,
    width: '85%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
  },
  flexBetween: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dateInput: {
    borderColor: '#9C9C9C',
    borderWidth: 1,
    borderRadius: 8,
    fontSize: 16,
    fontWeight: '400',
    padding: 10,
    paddingLeft: 35,
    paddingRight: 35,
    color: "black"
  },
});

export default StudioFilter;
