import {View, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';

const SearchCard = ({name, image, id}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('SearchAccount', {
          data: id,
        })
      }>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 20,
        }}>
        <View style={{flex: 0, flexDirection: 'row'}}>
          <View
            style={{
              height: 38,
              width: 38,
              alignSelf: 'center',
            }}>
            <Image
              style={{height: '100%', width: '100%', borderRadius: 100}}
              source={require('../../assets/images/test1.png')}
            />
          </View>

          <View style={{marginLeft: 20, alignSelf: 'center'}}>
            <Text
              style={{
                fontSize: 14,
                fontWeight: '700',
                color: '#2E2E2E',
              }}>
              {name}
            </Text>
          </View>
        </View>

        {/* <View style={{height: 37}}>
        <TouchableOpacity
          style={{padding: 10, backgroundColor: '#183761', borderRadius: 8}}>
          <Text style={{fontSize: 14, fontWeight: '500', color: '#FFFFFF'}}>
            Join club
          </Text>
        </TouchableOpacity>
      </View> */}
      </View>
    </TouchableOpacity>
  );
};

export default SearchCard;
