import {
  View,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Modal,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {getProfile} from '../../Service/Api';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import HubCard from '../Account/HubCard';
import AntDesign from 'react-native-vector-icons/AntDesign';

const SearchAccount = ({route}) => {
  const getprofileData = route?.params?.data;
  const navigation = useNavigation();
  const [otherLinks, setOtherLinks] = useState(false);
  const [tab, setTab] = useState('allPosts');
  const [profileData, setProfileData] = useState({});
  const [images, setImages] = useState({});
  const {user} = useSelector(state => state.data);
  const role = user.message;

  useEffect(() => {
    handleProfile();
  }, []);

  const handleProfile = async () => {
    const response = await getProfile(getprofileData);
    console.log('Profile', response);
    setProfileData(response);
  };

  return (
    <View
      style={{
        padding: 20,
        backgroundColor: '#FDFDFD',
        height: '100%',
        position: 'relative',
      }}>
      <View>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <AntDesign name="arrowleft" size={24} color="#183761" />
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={{
                fontSize: 20,
                fontWeight: '700',
                color: '#0C1317',
                marginLeft: -20,
              }}>
              {profileData.ownerName}
            </Text>
          </View>
          <View>{}</View>
        </View>
      </View>
      <View style={{height: '100%'}}>
        <ScrollView stickyHeaderIndices={[2]}>
          <View>
            <View
              style={{
                marginTop: 20,
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Image
                style={{
                  width: 80,
                  height: 80,
                  borderRadius: 100,
                }}
                source={require('../../assets/images/profileImage.png')}
              />
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  width: '70%',
                }}>
                <View>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontSize: 40,
                      fontWeight: '700',
                    }}>
                    10
                  </Text>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontSize: 14,
                      fontWeight: '400',
                      textAlign: 'center',
                    }}>
                    Posts
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontSize: 40,
                      fontWeight: '700',
                    }}>
                    132
                  </Text>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontSize: 14,
                      fontWeight: '400',
                      textAlign: 'center',
                    }}>
                    Club
                  </Text>
                </View>
              </View>
            </View>
            <View style={{marginTop: 20}}>
              <View>
                <Text
                  style={{color: '#2E2E2E', fontSize: 20, fontWeight: '700'}}>
                  {profileData.ownerName}
                </Text>
              </View>
              <View style={{width: '85%', marginTop: 10}}>
                <Text
                  style={{
                    color: '#2E2E2E',
                    fontSize: 14,
                    fontWeight: '400',
                  }}>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Felis donec et odio pellentesque diam.
                </Text>
                <Text
                  style={{
                    color: '#2E2E2E',
                    fontSize: 14,
                    fontWeight: '400',
                    marginTop: 10,
                  }}>
                  Tag : photography, videography, candid photography, drone,
                  video editing, photograhy album,
                </Text>
                <Text
                  style={{
                    color: '#2E2E2E',
                    fontSize: 14,
                    fontWeight: '400',
                    marginTop: 10,
                  }}>
                  {/* Address : 1/32. Guruji nagar, guindy, Chennai - 32. */}
                  {profileData.address}
                </Text>
              </View>
              <View
                style={{
                  flexL: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  marginTop: 10,
                }}>
                <View style={styles.socialIcon}>
                  <TouchableWithoutFeedback>
                    <Image
                      source={require('../../assets/images/websiteIcon.png')}
                    />
                  </TouchableWithoutFeedback>
                </View>
                <View style={styles.socialIcon}>
                  <TouchableWithoutFeedback>
                    <Image
                      style={{height: 28, width: 28}}
                      source={require('../../assets/images/whatsapp.png')}
                    />
                  </TouchableWithoutFeedback>
                </View>
                <View style={styles.socialIcon}>
                  <TouchableWithoutFeedback>
                    <Image
                      source={require('../../assets/images/facebook.png')}
                    />
                  </TouchableWithoutFeedback>
                </View>
                <View style={styles.socialIcon}>
                  <TouchableWithoutFeedback>
                    <Image
                      source={require('../../assets/images/twitter.png')}
                    />
                  </TouchableWithoutFeedback>
                </View>
                <View style={styles.socialIcon}>
                  <TouchableWithoutFeedback>
                    <Image
                      source={require('../../assets/images/instagram.png')}
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                {/* <View>
                    <TouchableWithoutFeedback
                      onPress={() => setOtherLinks(!otherLinks)}>
                      <View>
                        <Text
                          style={{
                            color: '#183761',
                            fontSize: 14,
                            fontWeight: '700',
                          }}>
                          View 2 other links
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  </View> */}
                {!profileData.shared && (
                  <View
                    style={{
                      justifyContent: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate('AddPhotos', {
                          data: getprofileData,
                        })
                      }>
                      <View style={styles.socialImage}>
                        <Text
                          style={{
                            color: '#183761',
                            fontSize: 16,
                            fontWeight: '400',
                          }}>
                          Phoub Book
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            </View>
          </View>

          <View style={{backgroundColor: '#FFFFFF'}}>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginTop: 10,
              }}>
              <TouchableWithoutFeedback onPress={() => setTab('allPosts')}>
                <View
                  style={
                    tab == 'allPosts' ? styles.selectTab : styles.unSelectTab
                  }>
                  <Text
                    style={
                      tab == 'allPosts'
                        ? styles.selectTabText
                        : styles.unSelectTabText
                    }>
                    All Posts
                  </Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => setTab('hub')}>
                <View
                  style={tab == 'hub' ? styles.selectTab : styles.unSelectTab}>
                  <Text
                    style={
                      tab == 'hub'
                        ? styles.selectTabText
                        : styles.unSelectTabText
                    }>
                    Hub
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
          <View style={{marginTop: 10}}>
            {tab == 'allPosts' ? (
              <View>
                <View style={{}}>
                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg1.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg2.png')}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg3.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg4.png')}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg1.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg2.png')}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg3.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg4.png')}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg1.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg2.png')}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg3.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg4.png')}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg1.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg2.png')}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg3.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg4.png')}
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg1.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg2.png')}
                      />
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginBottom: 10,
                    }}>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg3.png')}
                      />
                    </View>
                    <View style={{width: '48%'}}>
                      <Image
                        style={{width: '100%', borderRadius: 8}}
                        source={require('../../assets/images/testImg4.png')}
                      />
                    </View>
                  </View>
                </View>
              </View>
            ) : (
              <View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
                <View>
                  <HubCard />
                </View>
              </View>
            )}
          </View>
        </ScrollView>
        <View style={styles.container}>
          <Modal
            style={{height: 200}}
            transparent={true}
            animationType="slide"
            visible={otherLinks}
            onRequestClose={() => {
              setOtherLinks(!otherLinks);
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontSize: 20,
                      fontWeight: '700',
                    }}>
                    Other links
                  </Text>
                </View>
                <View
                  style={{
                    flex: 0,
                    flexDirection: 'row',
                    width: '100%',
                    marginTop: 20,
                  }}>
                  <View>
                    <Image
                      source={require('../../assets/images/whatsapp.png')}
                    />
                  </View>
                  <View style={{marginLeft: 20}}>
                    <Text
                      style={{
                        color: '#183761',
                        fontSize: 16,
                        fontWeight: '600',
                      }}>
                      https://wa.com/askjfhsjfpkuhl_id=1
                    </Text>
                  </View>
                </View>
                <View style={{flex: 0, flexDirection: 'row', marginTop: 15}}>
                  <View style={{justifyContent: 'center'}}>
                    <Icon name="email" color="#183761" size={16} />
                  </View>
                  <View style={{marginLeft: 20}}>
                    <Text
                      style={{
                        color: '#183761',
                        fontSize: 16,
                        fontWeight: '600',
                      }}>
                      https://wa.com/askjfhsjfpkuhl_id=1
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 22,
    height: 400,
  },
  socialIcon: {
    padding: 10,
    height: 56,
    width: 56,
    shadowColor: 'gray',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 0.7,
    shadowRadius: 10,
    elevation: 5,
  },
  socialImage: {
    shadowColor: '#7487A0',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 0.7,
    shadowRadius: 10,
    elevation: 5,
  },
  imageStyle: {
    height: 26,
    width: 26,
  },
  selectTab: {
    padding: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#183761',
    width: '48%',
  },
  unSelectTab: {
    padding: 10,
    width: '48%',
  },
  selectTabText: {
    fontSize: 16,
    fontWeight: '600',
    color: '#183761',
    textAlign: 'center',
  },
  unSelectTabText: {
    fontSize: 16,
    fontWeight: '400',
    color: '#183761',
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});

export default SearchAccount;
