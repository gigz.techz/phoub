import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {useDispatch} from 'react-redux';
import {saveStudioService} from '../../Service/redux/reducer/dataSlice';
import {useNavigation} from '@react-navigation/native';

const ServiceStudio = () => {
  const [selectedItems, setSelectedItems] = useState([]);
  const [othersService, setOthersService] = useState('');
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleSelectItem = item => {
    if (selectedItems.includes(item)) {
      setSelectedItems(selectedItems.filter(i => i !== item));
    } else {
      setSelectedItems([...selectedItems, item]);
    }
  };

  const isItemSelected = item => {
    return selectedItems.includes(item);
  };
  const handleService = () => {
    let updatedItems = [...selectedItems];
    updatedItems.push(othersService);
    const othersIndex = updatedItems.indexOf('others');
    const othersServiceIndex = updatedItems.indexOf({othersService});
    if (!isItemSelected('others')) {
      updatedItems.splice(othersServiceIndex, 1);
    }
    if (othersIndex !== -1) {
      updatedItems.splice(othersIndex, 1);
    }
    dispatch(saveStudioService(updatedItems));
    navigation.goBack();
  };
  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <ScrollView>
          {/* <View style={styles.searchContainer}>
          <TextInput
            style={styles.searchInput}
            placeholder="Type of service you offer"
            placeholderTextColor="#D1D7DF"
          />
          <View style={styles.crossIcon}>
            <Entypo name="cross" size={25} color="#D1D7DF" />
          </View>
        </View> */}
          <View style={{marginTop: 10}}>
            <Text style={styles.head}>Services</Text>
          </View>
          <View style={[styles.inputContainer, {marginBottom: 100}]}>
            <TouchableOpacity onPress={() => handleSelectItem('Videography')}>
              <View
                style={
                  isItemSelected('Videography')
                    ? styles.selectBox
                    : styles.unSelectBox
                }>
                <View style={{justifyContent: 'center'}}>
                  <Entypo
                    name="plus"
                    size={15}
                    color={
                      isItemSelected('Videography') ? '#183761' : '#FFFFFF'
                    }
                  />
                </View>
                <View style={{marginLeft: 10}}>
                  <Text
                    style={
                      isItemSelected('Videography')
                        ? styles.selectText
                        : styles.unSelectText
                    }>
                    Videography
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleSelectItem('Photography')}>
              <View
                style={
                  isItemSelected('Photography')
                    ? styles.selectBox
                    : styles.unSelectBox
                }>
                <View style={{justifyContent: 'center'}}>
                  <Entypo
                    name="plus"
                    size={15}
                    color={
                      isItemSelected('Photography') ? '#183761' : '#FFFFFF'
                    }
                  />
                </View>
                <View style={{marginLeft: 10}}>
                  <Text
                    style={
                      isItemSelected('Photography')
                        ? styles.selectText
                        : styles.unSelectText
                    }>
                    Photography
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleSelectItem('Photo editing')}>
              <View
                style={
                  isItemSelected('Photo editing')
                    ? styles.selectBox
                    : styles.unSelectBox
                }>
                <View style={{justifyContent: 'center'}}>
                  <Entypo
                    name="plus"
                    size={15}
                    color={
                      isItemSelected('Photo editing') ? '#183761' : '#FFFFFF'
                    }
                  />
                </View>
                <View style={{marginLeft: 10}}>
                  <Text
                    style={
                      isItemSelected('Photo editing')
                        ? styles.selectText
                        : styles.unSelectText
                    }>
                    Photo editing
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleSelectItem('Album designing')}>
              <View
                style={
                  isItemSelected('Album designing')
                    ? styles.selectBox
                    : styles.unSelectBox
                }>
                <View style={{justifyContent: 'center'}}>
                  <Entypo
                    name="plus"
                    size={15}
                    color={
                      isItemSelected('Album designing') ? '#183761' : '#FFFFFF'
                    }
                  />
                </View>
                <View style={{marginLeft: 10}}>
                  <Text
                    style={
                      isItemSelected('Album designing')
                        ? styles.selectText
                        : styles.unSelectText
                    }>
                    Album designing
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleSelectItem('Video editing')}>
              <View
                style={
                  isItemSelected('Video editing')
                    ? styles.selectBox
                    : styles.unSelectBox
                }>
                <View style={{justifyContent: 'center'}}>
                  <Entypo
                    name="plus"
                    size={15}
                    color={
                      isItemSelected('Video editing') ? '#183761' : '#FFFFFF'
                    }
                  />
                </View>
                <View style={{marginLeft: 10}}>
                  <Text
                    style={
                      isItemSelected('Video editing')
                        ? styles.selectText
                        : styles.unSelectText
                    }>
                    Video editing
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleSelectItem('Drone')}>
              <View
                style={
                  isItemSelected('Drone')
                    ? styles.selectBox
                    : styles.unSelectBox
                }>
                <View style={{justifyContent: 'center'}}>
                  <Entypo
                    name="plus"
                    size={15}
                    color={isItemSelected('Drone') ? '#183761' : '#FFFFFF'}
                  />
                </View>
                <View style={{marginLeft: 10}}>
                  <Text
                    style={
                      isItemSelected('Drone')
                        ? styles.selectText
                        : styles.unSelectText
                    }>
                    Drone
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleSelectItem('others')}>
              <View
                style={
                  isItemSelected('others')
                    ? styles.selectBox
                    : styles.unSelectBox
                }>
                <View style={{justifyContent: 'center'}}>
                  <Entypo
                    name="plus"
                    size={15}
                    color={isItemSelected('others') ? '#183761' : '#FFFFFF'}
                  />
                </View>
                <View style={{marginLeft: 10}}>
                  <Text
                    style={
                      isItemSelected('others')
                        ? styles.selectText
                        : styles.unSelectText
                    }>
                    Add others
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            {isItemSelected('others') && (
              <View style={[styles.unSelectBox, {padding: 7}]}>
                <TextInput
                  placeholder="Please Mention"
                  placeholderTextColor="#FFFFFF"
                  style={{color: '#FFFFFF'}}
                  value={othersService}
                  onChangeText={setOthersService}
                />
              </View>
            )}
          </View>
        </ScrollView>
      </View>
      <TouchableOpacity
        style={styles.createButton}
        onPress={() => handleService()}>
        <Text style={styles.createButtonText}>Done</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#183761',
    color: 'white',
    padding: 10,
    height: '100%',
  },
  head: {
    color: '#FEFEFE',
    fontSize: 36,
    fontWeight: '600',
    lineHeight: 65,
  },
  main: {
    position: 'relative',
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#D1D7DF',
    paddingBottom: 5,
    padding: 10,
  },
  searchInput: {
    flex: 1,
    color: '#D1D7DF',
    fontSize: 16,
    fontWeight: '400',
    paddingTop: 50,
  },
  crossIcon: {
    marginLeft: 10,
    paddingTop: 45,
  },
  inputContainer: {
    marginTop: 20,
  },
  unSelectBox: {
    flex: 0,
    flexDirection: 'row',
    color: 'white',
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#FFFFFF',
    padding: 20,
  },
  selectBox: {
    flex: 0,
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#FFFFFF',
    padding: 20,
  },
  icon: {
    marginRight: 10,
  },
  inputText: {
    flex: 1,
    color: 'white',
  },
  buttonText: {
    color: '#183761',
    fontSize: 16,
  },
  createButton: {
    position: 'absolute',
    bottom: 20,
    right: 10,
    left: 10,
    width: '100%',
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 15,
  },
  createButtonText: {
    fontSize: 20,
    color: '#183761',
    fontWeight: '600',
  },
  selectText: {
    color: '#183761',
    fontSize: 14,
    fontWeight: '600',
  },
  unSelectText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontWeight: '600',
  },
});

export default ServiceStudio;
