import { View, Text, TouchableOpacity, Image } from "react-native";
import React from "react";
import Icon from "react-native-vector-icons/Entypo";

const HubCard = () => {
  return (
    <View
      style={{
        flex: 0,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 20,
      }}
    >
      <View>
        <TouchableOpacity>
          <View style={{ flex: 0, flexDirection: "row" }}>
            <View style={{ width: 38, height: 38 }}>
              <Image
                style={{ borderRadius: 100, height: "100%", width: "100%" }}
                source={require("../../assets/images/back-display.png")}
              />
            </View>
            <View style={{ marginLeft: 20, justifyContent: "center" }}>
              <Text
                style={{ color: "#2E2E2E", fontSize: 14, fontWeight: "700" }}
              >
                Sanjay Bansali
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{ justifyContent: "center" }}>
        <TouchableOpacity>
          <Icon name="dots-three-vertical" size={16} color="#183761" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HubCard;
