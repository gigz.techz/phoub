import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  FlatList,
  Dimensions,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';
import Octicons from 'react-native-vector-icons/Feather';
import {addPhoubPhoto, getImagePath, getPhoubPhoto} from '../../Service/Api';
import DocumentPicker from 'react-native-document-picker';
import {useSelector} from 'react-redux';

const AddPhotos = ({route}) => {
  const userId = route?.params?.data;
  console.log('roleUSERID', userId);
  const navigation = useNavigation();
  const [image, setImage] = useState('');
  const [imagePath, setImagePath] = useState('');
  const [allPhotos, setAllPhotos] = useState([]);
  const {user} = useSelector(state => state.data);
  const role = user?.userId;
  console.log('role', role);

  useEffect(() => {
    handleGetPhoub();
    image && handleImage(image);
  }, [image]);

  const handleGetPhoub = async () => {
    const response = await getPhoubPhoto();
    setAllPhotos(response.photoPaths);
  };

  const handleImage = async item => {
    const response = await getImagePath(item);
    // setImagePath(response.path);
    response.path && handleAddPhoto(response.path);
  };

  const handleAddPhoto = async item => {
    const response = await addPhoubPhoto(item);
    handleGetPhoub();
  };

  const openDocumentFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      setImage(res);
    } catch (err) {
      if (DocumentPicker.isCancle(err)) {
      } else {
        throw err;
      }
    }
  };

  return (
    <View style={{height: '100%', position: 'relative'}}>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: '#FFFFFF',
          padding: 20,
        }}>
        <View style={{justifyContent: 'center'}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color="#183761" />
          </TouchableOpacity>
        </View>
        <View style={{justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#0C1317',
            }}>
            Photos
          </Text>
        </View>
        <View>
          {userId == role && (
            <TouchableOpacity onPress={() => navigation.navigate('ShareBook')}>
              <View
                style={{
                  backgroundColor: '#183761',
                  padding: 10,
                  borderRadius: 20,
                }}>
                <Text
                  style={{color: '#FFFFFF', fontSize: 16, fontWeight: '500'}}>
                  Share
                </Text>
              </View>
            </TouchableOpacity>
          )}
        </View>
      </View>
      <View style={{padding: 10, marginBottom: 100}}>
        <ScrollView>
          <View>
            <FlatList
              numColumns={2}
              data={allPhotos}
              renderItem={({item, index}) => {
                return (
                  <View key={index}>
                    <View
                      style={{
                        width: Dimensions.get('window').width / 2 - 20,
                        borderRadius: 16,
                        backgroundColor: '#FFFFFF',
                        margin: 5,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 15,
                      }}>
                      <Image
                        source={{uri: item.imagePath}}
                        style={{width: '100%', height: 150, borderRadius: 16}}
                      />
                    </View>
                  </View>
                );
              }}
            />
          </View>
        </ScrollView>
      </View>
      {userId == role && (
        <View style={{position: 'absolute', bottom: 20, right: 20}}>
          <TouchableOpacity onPress={() => openDocumentFile()}>
            <View
              style={{
                backgroundColor: '#183761',
                padding: 15,
                borderRadius: 50,
              }}>
              <Octicons name="upload" size={25} color="#FFFFFF" />
            </View>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

export default AddPhotos;
