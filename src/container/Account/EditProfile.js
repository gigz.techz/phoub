import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DocumentPicker from 'react-native-document-picker';
import {editProfile, getImagePath, getProfile} from '../../Service/Api';

const EditProfile = ({route}) => {
  const profileData1 = route && route?.params?.data;
  const [profileData2, setProfileData2] = useState({});

  console.log('EDOIITE PROD', profileData);
  useEffect(() => {
    handleProfile1();
  }, [profileData]);
  const handleProfile1 = async () => {
    const response = await getProfile();
    setProfileData2(response);
  };
  const profileData = profileData1 && profileData2;
  const navigation = useNavigation();
  const [image, setImage] = useState(null);
  const [name, setName] = useState('');
  const [mobile, setMobile] = useState('');
  const [studioName, setStudioName] = useState('');
  const [governmentRegistrationId, setGovernmentRegistrationId] = useState('');
  const [description, setDescription] = useState('');
  const [address, setAddress] = useState('');
  const [facebook, setFacebook] = useState('');
  const [youtube, setYoutube] = useState('');
  const [instagram, setInstagram] = useState('');
  const [twitter, setTwitter] = useState('');
  const [whatsapp, setWhatsapp] = useState('');
  const [email, setEmail] = useState('');
  const [website, setWebsite] = useState('');
  const [profileImage, setProfileImage] = useState('');
  const [socialMediaLinks, setSocialMediaLinks] = useState([]);

  useEffect(() => {
    setAddress(profileData?.address);
    setDescription(profileData?.description);
    setName(profileData?.ownerName);
    setGovernmentRegistrationId(profileData?.governmentRegistrationId);
    setStudioName(profileData?.studioName);
    handleSocialMedia(profileData?.socialMediaLinks);
    setSocialMediaLinks(profileData?.socialMediaLinks);
    setMobile(profileData?.mobileNumber);
  }, [profileData]);

  const handleSocialMedia = item => {
    item &&
      item.forEach(items => {
        if (items.platformName == 'facebook') {
          setFacebook(items.path);
        } else if (items.platformName == 'youtube') {
          setYoutube(items.path);
        } else if (items.platformName == 'instagram') {
          setInstagram(items.path);
        } else if (items.platformName == 'twitter') {
          setTwitter(items.path);
        } else if (items.platformName == 'whatsapp') {
          setWhatsapp(items.path);
        } else if (items.platformName == 'email') {
          setEmail(items.path);
        } else if (items.platformName == 'website') {
          setWebsite(items.path);
        }
      });
  };

  const openDocumentFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      setImage(res);
    } catch (err) {
      if (DocumentPicker.isCancle(err)) {
      } else {
        throw err;
      }
    }
  };

  useEffect(() => {
    image && handleImage(image);
  }, [image]);

  const handleImage = async item => {
    const response = await getImagePath(item);
    setProfileImage(response.path);
  };

  const handleEditProfile = async () => {
    const payload = {
      fullName: name,
      address: address,
      profilePicturePath: profileImage,
      mobileNumber: mobile,
      studioName: studioName,
      ownerName: name,
      governmentRegistrationId: governmentRegistrationId,
      description: description,
      socialMediaLinks: socialMediaLinks,
    };
    const response = await editProfile(payload);
    console.log('EDIT RES', response);
    // navigation.navigate('Parent');
    handleProfile();
  };

  const handleProfile = async () => {
    // const response = await getProfile();
    // navigation.navigate('Parent', {
    //   data: response,
    // });
    navigation.goBack();
  };

  const handleLinks = (name, value) => {
    console.log(name, value);

    const existingIndex = socialMediaLinks.findIndex(
      item => item.platformName === name,
    );

    if (existingIndex !== -1) {
      const updatedLinks = socialMediaLinks.map((item, index) =>
        index === existingIndex ? {...item, path: value} : item,
      );

      setSocialMediaLinks(updatedLinks);
      console.log('UPDATE', updatedLinks);
    } else {
      const newLinks = [...socialMediaLinks, {platformName: name, path: value}];

      setSocialMediaLinks(newLinks);
      console.log('ADD', newLinks);
    }
  };

  console.log('NEW LINKS', socialMediaLinks);

  return (
    <View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: '#fff',
          padding: 10,
        }}>
        <View style={{justifyContent: 'center'}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color="#183761" />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={{color: '#2E2E2E', fontSize: 20, fontWeight: '700'}}>
            Edit Profile
          </Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <TouchableOpacity onPress={() => handleEditProfile()}>
            <View
              style={{
                backgroundColor: '#183761',
                padding: 10,
                paddingLeft: 15,
                paddingRight: 15,
                borderRadius: 16,
              }}>
              <Text style={{fontSize: 12, fontWeight: '400', color: '#FFFFFF'}}>
                Done
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>
        <View style={{padding: 20, marginBottom: 50}}>
          <View style={{alignSelf: 'center', marginTop: 10}}>
            <View
              style={{
                backgroundColor: '#e3e3e3',
                borderRadius: 100,
                height: 108,
                width: 108,
              }}>
              {image ? (
                image.map((item, index) => (
                  <View key={index}>
                    <Image
                      style={{
                        resizeMode: 'contain',
                        height: 108,
                        width: 108,
                        borderRadius: 100,
                      }}
                      source={{uri: item.uri}}></Image>
                  </View>
                ))
              ) : (
                <Image
                  style={{
                    borderRadius: 100,
                    height: '100%',
                    width: '100%',
                    alignSelf: 'center',
                  }}
                  source={require('../../assets/images/profile.png')}></Image>
              )}
            </View>
            <View style={{alignSelf: 'center', marginTop: -15}}>
              <TouchableOpacity onPress={() => openDocumentFile()}>
                <View
                  style={{
                    backgroundColor: '#FFFFFF',
                    height: 30,
                    width: 30,
                    borderRadius: 100,
                  }}>
                  <Image
                    style={{
                      justifyContent: 'center',
                      height: '100%',
                      width: '100%',
                    }}
                    source={require('../../assets/images/upload.png')}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{marginTop: 20}}>
            <View>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Owner Name
              </Text>
              <TextInput
                style={styles.input}
                value={name}
                onChangeText={setName}
                placeholder="Enter Name"
              />
            </View>
            <View style={{marginTop: 10}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Mobile Number
              </Text>
              <TextInput
                style={styles.input}
                value={mobile}
                onChangeText={setMobile}
                placeholder="Enter Mobile"
              />
            </View>
            <View style={{marginTop: 10}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Studio Name
              </Text>
              <TextInput
                style={styles.input}
                value={studioName}
                onChangeText={setStudioName}
                placeholder="Enter Studio Name"
              />
            </View>

            <View style={{marginTop: 10}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Government Registration Id
              </Text>
              <TextInput
                style={styles.input}
                value={governmentRegistrationId}
                onChangeText={setGovernmentRegistrationId}
                placeholder="Enter Government Registration Id"
              />
            </View>
            <View style={{marginTop: 10}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Description
              </Text>
              <TextInput
                value={description}
                style={{
                  height: 150,
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Type your description..."
                placeholderTextColor="#9C9C9C"
                onChangeText={setDescription}
              />
            </View>
            <View style={{marginTop: 10}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Address
              </Text>
              <TextInput
                value={address}
                style={{
                  height: 80,
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Type your description..."
                placeholderTextColor="#9C9C9C"
                onChangeText={setAddress}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text
                style={{
                  fontSize: 18,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Social Media Links
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Facebook
              </Text>
              <TextInput
                value={facebook}
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Enter Facebook URL"
                placeholderTextColor="#9C9C9C"
                onChangeText={e => {
                  handleLinks('facebook', e);
                  setFacebook(e);
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Youtube
              </Text>
              <TextInput
                value={youtube}
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Enter Youtube link"
                placeholderTextColor="#9C9C9C"
                onChangeText={e => {
                  handleLinks('youtube', e);
                  setYoutube(e);
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Instagram
              </Text>
              <TextInput
                value={instagram}
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Enter Instagram link"
                placeholderTextColor="#9C9C9C"
                onChangeText={e => {
                  handleLinks('instagram', e);
                  setInstagram(e);
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Twitter
              </Text>
              <TextInput
                value={twitter}
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Enter Twitter link"
                placeholderTextColor="#9C9C9C"
                onChangeText={e => {
                  handleLinks('twitter', e);
                  setTwitter(e);
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Whatsapp
              </Text>
              <TextInput
                value={whatsapp}
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Enter WhatsApp number"
                placeholderTextColor="#9C9C9C"
                onChangeText={e => {
                  handleLinks('whatsapp', e);
                  setWhatsapp(e);
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Email
              </Text>
              <TextInput
                value={email}
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Enter your Email"
                placeholderTextColor="#9C9C9C"
                onChangeText={e => {
                  handleLinks('email', e);
                  setEmail(e);
                }}
              />
            </View>
            <View style={{marginTop: 20}}>
              <Text
                style={{
                  fontSize: 16,
                  color: 'black',
                  fontWeight: '400',
                  marginBottom: 5,
                }}>
                Website
              </Text>
              <TextInput
                value={website}
                style={{
                  fontSize: 16,
                  fontWeight: '400',
                  width: '100%',
                  backgroundColor: '#FFFFFF',
                  borderRadius: 8,
                }}
                placeholder="Enter your website"
                placeholderTextColor="#9C9C9C"
                onChangeText={e => {
                  handleLinks('website', e);
                  setWebsite(e);
                }}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 8,
    borderWidth: 1,
    backgroundColor: '#FFFFFF',
    padding: 10,
    borderColor: '#EBEBEB',
    color: 'black',
    fontSize: 16,
    fontWeight: '400',
  },
});

export default EditProfile;
