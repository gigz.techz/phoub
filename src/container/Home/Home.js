import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Modal,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import CustomHeader from '../../components/CustomHeader';
import {Calendar} from 'react-native-calendars';
import FeedCard from '../../components/FeedCard';
import TaskButton from './TaskButton';
import {
  deleteComment,
  editComment,
  getAllComment,
  getAllPost,
  getAllTask,
  getNotification,
  getUserRole,
  postComment,
} from '../../Service/Api';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch} from 'react-redux';
import {saveUser} from '../../Service/redux/reducer/dataSlice';
import {useNavigation} from '@react-navigation/native';
import moment from 'moment';
import randomColor from 'randomcolor';

const Home = ({}) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [selected, setSelected] = useState('');
  const [allPost, setAllPost] = useState([]);
  const [show, setShow] = useState(false);
  const [postId, setPostId] = useState('');
  const [comment, setComment] = useState('');
  const [commentId, setCommentId] = useState('');
  const [commentData, setCommentData] = useState({});
  const [edit, setEdit] = useState(false);
  const [allTask, setAllTask] = useState([]);
  const [notificationBell, setNotificationBell] = useState(false);
  const [notificationData, setNotificationData] = useState([]);

  useEffect(() => {
    handleAllPost();
    handleAllTask();
    handleUserRole();
  }, []);

  useEffect(() => {
    const intervalId = setInterval(() => {
      handleNotification();
    }, 5000);

    return () => clearInterval(intervalId);
  }, [handleNotification]);

  const handleUserRole = async () => {
    const response = await getUserRole();
    console.log('USREROLE', response);
    dispatch(saveUser(response));
  };

  const handleAllPost = async () => {
    const response = await getAllPost();
    console.log('All', response.posts);
    setAllPost(response.posts);
  };

  const handleShow = item => {
    setShow(!show);
    setPostId(item);
  };
  const handleAllComment = async item => {
    const response = await getAllComment(item);
    console.log('Comment', response);
    setCommentData(response);
  };

  const handleComment = async () => {
    if (edit == false) {
      const data = {
        postId: postId,
        commentContent: comment,
      };
      const response = await postComment(data);
      console.log('POst Comment', response);
      handleAllComment(postId);
      setComment('');
    } else {
      const data = {
        commentId: commentId,
        commentContent: comment,
      };
      const response = await editComment(data);
      setEdit(false);
      console.log('EDIT', response);
      handleAllComment(postId);
      setComment('');
    }
  };

  const handleDeleteComment = async item => {
    const response = await deleteComment(item);
    console.log('DLETE RESPO', response);
    handleAllComment(postId);
  };

  const convertedEmail = email => {
    const atIndex = email.indexOf('@');
    if (atIndex !== -1) {
      const username = email.substring(0, atIndex);
      return `@${username}`;
    } else {
      return email;
    }
  };

  const handleAllTask = async () => {
    const response = await getAllTask();
    console.log('ALL TASK', response);
    setAllTask(response?.tasks);
  };

  // const generateRandomLightColor = () => {
  //   const letters = '0123456789ABCDEF';
  //   let color = '#';
  //   for (let i = 0; i < 6; i++) {
  //     color += letters[Math.floor(Math.random() * 16)];
  //   }
  //   return color;
  // };

  const generateRandomDarkColors = () => {
    const color = randomColor({luminosity: 'dark'});
    return color;
  };

  const getRandomElementFromArray = arr => {
    const randomIndex = Math.floor(Math.random() * arr.length);
    return arr[randomIndex];
  };

  const myArray = [
    '#ef15cb',
    '#3e8e05',
    '#0b7c25',
    '#001a5e',
    '#8c07bc',
    '#014066',
    '#0e8c6e',
    '#82032d',
  ];

  const handleOpenNotification = data => {
    navigation.navigate('NotificationScreen', {
      data: notificationData,
    });
  };

  const handleNotification = async () => {
    const response = await getNotification();

    if (response.notification.length > 0) {
      setNotificationData(response);
      setNotificationBell(true);
    } else {
      setNotificationBell(false);
    }

    // handleOpenNotification(response);
  };

  const formatDate = inputDate => {
    const formattedDate = moment(inputDate).format('YYYY-MM-DD');
    return formattedDate;
  };

  return (
    <View style={{padding: 20, backgroundColor: '#FDFDFD', height: '100%'}}>
      <View>
        <CustomHeader
          onPress={() => handleOpenNotification()}
          bell={notificationBell}
        />
      </View>
      <View style={{position: 'relative'}}>
        <ScrollView>
          <View>
            <View
              style={{
                marginTop: 30,
                backgroundColor: '#FDFDFD',
                padding: 5,
              }}>
              <Text
                onPress={() => navigation.navigate('AllTaskScreen')}
                style={{
                  color: '#A3AFC0',
                  fontSize: 16,
                  fontWeight: '400',
                }}>
                Schedule
              </Text>
            </View>
            <View>
              <Calendar
                // theme={{stylesheet: {
                //   day: {
                //     fontSize: 18,
                //   },
                // }}}
                onMonthChange={() => handleAllTask()}
                onDayPress={day => {
                  if (day.dateString >= formatDate(new Date())) {
                    setSelected(day.dateString);
                    navigation.navigate('CalendarScreen', {
                      data: {...day},
                    });
                  }
                }}
                markedDates={
                  allTask &&
                  allTask.reduce((marked, item) => {
                    if (formatDate(item.dueDate) <= formatDate(new Date())) {
                      marked[formatDate(item.dueDate)] = {
                        selected: true,
                        selectedColor: '#b5f5a6',
                        selectedTextColor: '#FFFFFF',
                        shadowOpacity: 0.5,
                      };
                    } else {
                      marked[formatDate(item.dueDate)] = {
                        selected: true,
                        selectedColor: getRandomElementFromArray(myArray),
                        selectedTextColor: '#FFFFFF',
                      };
                    }
                    return marked;
                  }, {})
                }
              />
              {/* // markedDates={{
                //   [selected]: {
                //     selected: true,
                //     disableTouchEvent: true,
                //     selectedDotColor: 'orange',
                //   },
                //   '2024-01-12': {
                //     selected: true,
                //     selectedColor: '#aa2222',
                //     selectedTextColor: 'yellow',
                //   },
                // }} */}
            </View>
          </View>
          <View>
            <View
              style={{
                marginBottom: 5,
                marginTop: 30,
                backgroundColor: '#FDFDFD',
              }}>
              <Text
                style={{
                  color: '#A3AFC0',
                  fontSize: 16,
                  fontWeight: '400',
                }}>
                Your Feed
              </Text>
            </View>
          </View>
          <View style={{marginBottom: 30}}>
            {allPost &&
              allPost.map(item => (
                <View>
                  <FeedCard
                    item={item}
                    getAllPost={handleAllPost}
                    handleShow={handleShow}
                    handleAllComment={handleAllComment}
                  />
                </View>
              ))}
          </View>
          <View style={{marginBottom: 50}}></View>
        </ScrollView>
      </View>
      <View style={{position: 'absolute', bottom: 60, right: 20}}>
        <TaskButton />
      </View>
      <View>
        <Modal
          style={{height: 200}}
          transparent={true}
          animationType="slide"
          visible={show}
          onRequestClose={() => {
            setShow(!show);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View>
                <View style={{height: 300}}>
                  <TouchableOpacity onPress={() => setShow(false)}>
                    <View
                      style={{
                        height: 4,
                        width: 80,
                        backgroundColor: 'gray',
                        alignSelf: 'center',
                        marginTop: -18,
                      }}></View>
                  </TouchableOpacity>
                  <ScrollView>
                    <View>
                      {commentData &&
                        commentData.comments &&
                        commentData.comments.map((item, i) => (
                          <View
                            key={i}
                            style={{
                              flex: 0,
                              flexDirection: 'row',
                              position: 'relative',
                              marginBottom: 10,
                            }}>
                            <View>
                              {/* <Image
                                style={{
                                  height: 40,
                                  width: 40,
                                  borderRadius: 50,
                                }}
                                source={{uri: item.profilePic}}
                              /> */}
                              <Image
                                style={{
                                  height: 40,
                                  width: 40,
                                  borderRadius: 50,
                                }}
                                source={require('../../assets/images/back-display.png')}
                              />
                            </View>
                            <View style={{marginLeft: 20}}>
                              <View>
                                <Text
                                  style={{
                                    fontSize: 16,
                                    fontWeight: '600',
                                    color: '#2E2E2E',
                                  }}>
                                  {convertedEmail(item.username)}
                                </Text>
                              </View>
                              <View>
                                <Text
                                  style={{
                                    fontSize: 14,
                                    fontWeight: '400',
                                    color: '#2E2E2E',
                                  }}>
                                  {item.commentText}
                                </Text>
                              </View>
                            </View>
                            <View
                              style={{
                                position: 'absolute',
                                top: 0,
                                right: 0,
                                flex: 0,
                                flexDirection: 'row',
                                gap: 5,
                              }}>
                              <TouchableOpacity
                                onPress={() => {
                                  setEdit(true);
                                  setCommentId(item.commentId);
                                  setComment(item.commentText);
                                }}>
                                <Icon name="edit" size={15} color="gray" />
                              </TouchableOpacity>
                              <TouchableOpacity
                                onPress={() =>
                                  handleDeleteComment(item.commentId)
                                }>
                                <Icon name="delete" size={15} color="#183761" />
                              </TouchableOpacity>
                            </View>
                          </View>
                        ))}
                    </View>
                  </ScrollView>
                </View>
                <View
                  style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={[styles.searchContainer, {borderColor: '#D1D7DF'}]}>
                    <TextInput
                      value={comment}
                      style={styles.input}
                      placeholder="Add Comment"
                      onChangeText={setComment}
                    />
                  </View>
                  <View style={{justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => handleComment()}>
                      <Ionicons name="send" size={24} color="#183761" />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 40,
    height: 40,
    paddingHorizontal: 10,
    width: '90%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
    color: 'black',
  },
});

export default Home;
