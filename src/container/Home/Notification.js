import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {getNotification, seenNotification} from '../../Service/Api';

const Notification = ({route}) => {
  const notificationList = route?.params?.data;
  const [notifications, setNotifications] = useState({});
  const navigation = useNavigation();
  useEffect(() => {
    setNotifications(notificationList);
  }, [notificationList]);
  const handleSeenNotification = async value => {
    const response = await seenNotification(value);
    handleUpdateNotification();
  };
  const handleUpdateNotification = async () => {
    const response = await getNotification();
    setNotifications(response);
  };

  return (
    <View style={{padding: 20}}>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color='#183761' />
          </TouchableOpacity>
        </View>
        <View>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#0C1317',
              marginLeft: -20,
            }}>
            Notification
          </Text>
        </View>
        <View>{}</View>
      </View>
      <ScrollView>
        <View style={{marginTop: 20}}>
          {notifications &&
            notifications.notification &&
            notifications.notification.map((item, i) => (
              <View key={i} style={{marginBottom: 10}}>
                <TouchableOpacity
                  onPress={() => handleSeenNotification(item.id)}>
                  <View
                    style={{
                      padding: 20,
                      backgroundColor: '#FFFFFF',
                      borderRadius: 10,
                    }}>
                    <Text style={{color: 'black'}}>{item.message}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default Notification;
