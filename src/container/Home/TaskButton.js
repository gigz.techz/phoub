import {useNavigation} from '@react-navigation/native';
import React, {useState, useEffect} from 'react';
import {StyleSheet, View, TouchableOpacity, Animated} from 'react-native';
import Icons from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/FontAwesome';

const TaskButton = () => {
  const navigation = useNavigation();
  const [menuOpen, setMenuOpen] = useState(false);
  const [showPlusButton, setShowPlusButton] = useState(false);
  const [showCustomButton, setShowCustomButton] = useState(false);
  const animation = new Animated.Value(0);

  const toggleMenu = () => {
    const toValue = menuOpen ? 0 : 1;
    Animated.spring(animation, {
      toValue,
      friction: 5,
      useNativeDriver: true,
    }).start();
    setMenuOpen(!menuOpen);
    setShowPlusButton(!menuOpen);
    setShowCustomButton(!menuOpen);
  };

  useEffect(() => {
    animation.setValue(menuOpen ? 1 : 0);
  }, [menuOpen]);

  const rotateAnimation = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '-45deg'],
  });

  const menuStyle = {
    transform: [
      {
        scale: animation.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 1.2],
        }),
      },
    ],
  };

  const handlePlusButtonClick = () => {
    navigation.navigate('NotesPage');
  };
  const handleCustomButtonClick = () => {};
  const handleThirdButtonClick = () => {
    setMenuOpen(false);
  };
  return (
    <View style={styles.container}>
      <View style={styles.buttonsContainer}>
        <View>
          <TouchableOpacity
            onPress={toggleMenu}
            style={[styles.button, menuOpen && styles.openButton]}>
            <Animated.View style={{transform: [{rotate: rotateAnimation}]}}>
              <Icon name="clone" size={20} color="white" />
            </Animated.View>
          </TouchableOpacity>
        </View>
        {menuOpen && (
          <View style={styles.additionalButtonsContainer}>
            {showPlusButton && (
              <TouchableOpacity
                onPress={handlePlusButtonClick}
                style={[styles.additionalButton, {backgroundColor: 'white'}]}>
                <Icon name="folder" size={20} color="red" />
              </TouchableOpacity>
            )}
            {showCustomButton && (
              <TouchableOpacity
                onPress={handleCustomButtonClick}
                style={[styles.additionalButton, {backgroundColor: 'white'}]}>
                <Icon name="folder-open" size={20} color="orange" />
              </TouchableOpacity>
            )}
            <TouchableOpacity
              onPress={handleThirdButtonClick}
              style={[styles.cross]}>
              <Icons name="cross" size={25} color="white" />
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#183761',
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 3.84,
    elevation: 5,
    marginLeft: 16,
  },

  additionalButtonsContainer: {
    flexDirection: 'row',
    marginLeft: 20,
    backgroundColor: '#183761',
    borderRadius: 40,
    padding: 5,
    overflow: 'hidden',
    position: 'absolute',
    right: 0,
    height: 60,
  },
  additionalButton: {
    width: 50,
    height: 50,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 3.84,
    elevation: 5,
    marginLeft: 16,
  },
  cross: {
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default TaskButton;
