import {View, Text, StyleSheet, ScrollView, Image} from 'react-native';
import React from 'react';
import CustomHeader2 from '../../components/CustomHeader2';

const AllTaskScreen = ({navigation}) => {
  return (
    <View style={{padding: 10}}>
      <View>
        <CustomHeader2 text="All Task Lists" navigation={navigation} />
      </View>
      <View>
        <ScrollView>
          <View style={{marginBottom: 100}}>
            <View>
              <View
                style={{
                  marginTop: 30,
                  padding: 5,
                }}>
                <Text
                  style={{
                    color: '#A3AFC0',
                    fontSize: 16,
                    fontWeight: '400',
                  }}>
                  Sat 7, November, 2023
                </Text>
              </View>
              <View style={{padding: 20}}>
                <View>
                  <View>
                    <Text style={styles.listText}>1. Birthday Ceremony</Text>
                  </View>
                  <View style={{flex: 0, flexDirection: 'row', marginTop: 10}}>
                    <View>
                      <Image
                        style={{width: 24, height: 24, borderRadius: 100}}
                        source={require('../../assets/images/back-display.png')}
                      />
                    </View>
                    <View style={{marginLeft: 10}}>
                      <Text
                        style={{
                          fontSize: 14,
                          color: '#2E2E2E',
                          fontWeight: '400s',
                        }}>
                        Karthik
                      </Text>
                    </View>
                  </View>
                </View>
                <View>
                  <Image />
                </View>
              </View>
              <View style={{padding: 20}}>
                <View>
                  <View>
                    <Text style={styles.listText}>2. Housing Ceremony</Text>
                  </View>
                  <View style={{flex: 0, flexDirection: 'row', marginTop: 10}}>
                    <View>
                      <Image
                        style={{width: 24, height: 24, borderRadius: 100}}
                        source={require('../../assets/images/back-display.png')}
                      />
                    </View>
                    <View style={{marginLeft: 10}}>
                      <Text
                        style={{
                          fontSize: 14,
                          color: '#2E2E2E',
                          fontWeight: '400s',
                        }}>
                        Partha
                      </Text>
                    </View>
                  </View>
                </View>
                <View>
                  <Image />
                </View>
              </View>
              <View style={{padding: 20}}>
                <View>
                  <View>
                    <Text style={styles.listText}>3. Marriage Function</Text>
                  </View>
                  <View style={{flex: 0, flexDirection: 'row', marginTop: 10}}>
                    <View>
                      <Image
                        style={{width: 24, height: 24, borderRadius: 100}}
                        source={require('../../assets/images/back-display.png')}
                      />
                    </View>
                    <View style={{marginLeft: 10}}>
                      <Text
                        style={{
                          fontSize: 14,
                          color: '#2E2E2E',
                          fontWeight: '400s',
                        }}>
                        Karthik
                      </Text>
                    </View>
                  </View>
                </View>
                <View>
                  <Image />
                </View>
              </View>
            </View>
            <View>
              <View
                style={{
                  marginTop: 20,
                  padding: 5,
                }}>
                <Text
                  style={{
                    color: '#A3AFC0',
                    fontSize: 16,
                    fontWeight: '400',
                  }}>
                  Tue 10, November, 2023
                </Text>
              </View>
              <View style={{padding: 20}}>
                <View>
                  <View>
                    <Text style={styles.listText}>2. Birthday Ceremony</Text>
                  </View>
                  <View style={{flex: 0, flexDirection: 'row', marginTop: 10}}>
                    <View>
                      <Image
                        style={{width: 24, height: 24, borderRadius: 100}}
                        source={require('../../assets/images/back-display.png')}
                      />
                    </View>
                    <View style={{marginLeft: 10}}>
                      <Text
                        style={{
                          fontSize: 14,
                          color: '#2E2E2E',
                          fontWeight: '400s',
                        }}>
                        Partha
                      </Text>
                    </View>
                  </View>
                </View>
                <View>
                  <Image />
                </View>
              </View>
              <View style={{padding: 20}}>
                <View>
                  <View>
                    <Text style={styles.listText}>2. Marriage Ceremony</Text>
                  </View>
                  <View style={{flex: 0, flexDirection: 'row', marginTop: 10}}>
                    <View>
                      <Image
                        style={{width: 24, height: 24, borderRadius: 100}}
                        source={require('../../assets/images/back-display.png')}
                      />
                    </View>
                    <View style={{marginLeft: 10}}>
                      <Text
                        style={{
                          fontSize: 14,
                          color: '#2E2E2E',
                          fontWeight: '400s',
                        }}>
                        Partha
                      </Text>
                    </View>
                  </View>
                </View>
                <View>
                  <Image />
                </View>
              </View>
            </View>
            <View>
              <View
                style={{
                  marginTop: 20,
                  padding: 5,
                }}>
                <Text
                  style={{
                    color: '#A3AFC0',
                    fontSize: 16,
                    fontWeight: '400',
                  }}>
                  Wed 11, November, 2023
                </Text>
              </View>
              <View style={{padding: 20}}>
                <View>
                  <View>
                    <Text style={styles.listText}>2. Birthday Ceremony</Text>
                  </View>
                  <View style={{flex: 0, flexDirection: 'row', marginTop: 10}}>
                    <View>
                      <Image
                        style={{width: 24, height: 24, borderRadius: 100}}
                        source={require('../../assets/images/back-display.png')}
                      />
                    </View>
                    <View style={{marginLeft: 10}}>
                      <Text
                        style={{
                          fontSize: 14,
                          color: '#2E2E2E',
                          fontWeight: '400s',
                        }}>
                        Partha
                      </Text>
                    </View>
                  </View>
                </View>
                <View>
                  <Image />
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listText: {
    fontSize: 20,
    fontWeight: '600',
    color: '#2E2E2E',
    marginBottom: 5,
  },
});

export default AllTaskScreen;
