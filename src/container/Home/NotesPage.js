import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import React, {useEffect, useState} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';

const NotesPage = () => {
  const navigation = useNavigation();
  const [text, setText] = useState('');
  useEffect(() => {
    handleSaveText();
  }, []);
  const handleSaveText = async () => {
    const value = await AsyncStorage.getItem('Text');
    value && setText(value);
  };
  const handeText = async value => {
    setText(value);
    await AsyncStorage.setItem('Text', value);
  };
  return (
    <View style={{}}>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: '#FFFFFF',
          padding: 20,
        }}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color='#183761' />
          </TouchableOpacity>
        </View>
        <View>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#0C1317',
              marginLeft: -20,
            }}>
            Notes
          </Text>
        </View>
        <View>{}</View>
      </View>
      <View style={{padding: 20, height: '100%'}}>
        <View style={{backgroundColor: '#FFFFFF', height: '100%'}}>
          <TextInput
            multiline
            style={{}}
            onChangeText={handeText}
            value={text}
          />
        </View>
      </View>
    </View>
  );
};

export default NotesPage;
