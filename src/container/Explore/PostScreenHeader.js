import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Ionicons';

const PostScreenHeader = ({text, onPress}) => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        padding: 10,
      }}>
      <View style={{justifyContent: 'center'}}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="arrowleft" size={24} color='183761' />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          padding: 10,
          backgroundColor: '#D8DFE3',
          borderRadius: 40,
          paddingLeft: 10,
          paddingRight: 10,
        }}>
        <Text>Notification</Text>
        <Icon
          name="notifications"
          size={20}
          color="#183761"
          style={{marginLeft: 10}}
        />
      </View>
      <View style={{justifyContent: 'center'}}>
        <TouchableOpacity onPress={onPress}>
          <View
            style={{
              backgroundColor: '#183761',
              padding: 10,
              paddingLeft: 15,
              paddingRight: 15,
              borderRadius: 16,
            }}>
            <Text style={{fontSize: 12, fontWeight: '400', color: '#FFFFFF'}}>
              {text}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default PostScreenHeader;
