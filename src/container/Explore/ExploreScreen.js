import {
  View,
  Text,
  Platform,
  PermissionsAndroid,
  FlatList,
  Dimensions,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  ScrollView,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {CameraRoll} from '@react-native-camera-roll/camera-roll';
import PostScreenHeader from './PostScreenHeader';
import Icon from 'react-native-vector-icons/Octicons';
import Carousel, {Pagination} from 'react-native-snap-carousel';

const ExploreScreen = ({navigation}) => {
  const [photos, setPhotos] = useState([]);
  const [index, setIndex] = useState(0);
  const [selectedItems, setSelectedItems] = useState([]);
  const [carouselData, setCarouselData] = useState([]);

  const isCarousel = useRef(null);

  useEffect(() => {
    hasPermission();
    getAllPhotos();
  }, []);

  const hasPermission = async () => {
    const permission =
      Platform.Version >= 33
        ? PermissionsAndroid.PERMISSIONS.READ_MEDIA_IMAGES
        : PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE;

    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
      return true;
    }

    const status = await PermissionsAndroid.request(permission);
    return status === 'granted';
  };

  const getAllPhotos = () => {
    CameraRoll.getPhotos({
      first: 20,
      assetType: 'Photos',
    })
      .then(r => {
        // console.log(JSON.stringify(r.edges));
        setPhotos(r.edges);
      })
      .catch(err => {
        //Error Loading Images
      });
  };

  const handleItemSelected = index => {
    const updatedSelection = [...selectedItems];
    updatedSelection[index] = !updatedSelection[index];
    setSelectedItems(updatedSelection);
  };

  useEffect(() => {
    updateCarouselData();
  }, [selectedItems]);

  const updateCarouselData = () => {
    const updatedCarouselData = photos.filter(
      (_, index) => selectedItems[index],
    );
    setCarouselData(updatedCarouselData);
  };

  const renderItem = ({item}) => (
    <View
      style={{
        marginTop: 20,
        alignItems: 'center',
        borderRadius: 10,
      }}>
      <Image
        source={{uri: item.node.image.uri}}
        style={{height: 268, width: '100%', borderRadius: 16}}
      />
    </View>
  );

  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF', padding: 10}}>
      <View>
        <PostScreenHeader
          text="Next"
          onPress={() =>
            navigation.navigate('PostDescriptionScreen', {
              data: {...carouselData},
            })
          }
        />
      </View>
      <ScrollView>
        <View style={{marginTop: 10}}>
          <Carousel
            ref={isCarousel}
            data={carouselData}
            renderItem={renderItem}
            sliderWidth={400}
            itemWidth={320}
            onSnapToItem={currentIndex => setIndex(currentIndex)}
          />
          <View style={{marginTop: -20}}>
            <Pagination
              dotsLength={carouselData.length}
              activeDotIndex={index}
              carouselRef={isCarousel}
              dotStyle={{
                width: 15,
                height: 10,
                borderRadius: 10,
                backgroundColor: 'rgb(116, 135, 160)',
              }}
            />
          </View>
        </View>
        <View style={{padding: 10, borderRadius: 25}}>
          <View
            style={{
              marginTop: 10,
              paddingLeft: 10,
              paddingRight: 10,
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={{flex: 0, flexDirection: 'row'}}>
              <View>
                <Text
                  style={{fontSize: 20, fontWeight: '700', color: '#2E2E2E'}}>
                  All media
                </Text>
              </View>
              <View
                style={{
                  borderRadius: 15,
                  backgroundColor: '#D8DFE3',
                  padding: 10,
                  alignItems: 'center',
                  marginLeft: 10,
                }}>
                <Icon name="file-directory" size={16} color="#183761" />
              </View>
            </View>
            <View>
              <TouchableOpacity onPress={() => setSelectedItems('')}>
                <Text
                  style={{fontSize: 14, fontWeight: '600', color: '#183761'}}>
                  Clear selection
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <FlatList
            numColumns={2}
            data={photos}
            renderItem={({item, index}) => {
              const isSelected = selectedItems[index];

              return (
                <View key={index}>
                  <TouchableOpacity
                    onPress={() => {
                      handleItemSelected(index);
                    }}>
                    <View
                      style={{
                        width: Dimensions.get('window').width / 2 - 20,
                        borderRadius: 16,
                        backgroundColor: '#FFFFFF',
                        margin: 5,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 15,
                      }}>
                      {isSelected ? (
                        <ImageBackground
                          resizeMode="cover"
                          source={{uri: item.node.image.uri}}
                          style={{
                            width: '100%',
                            height: 150,
                            borderRadius: 16,
                            position: 'relative',
                          }}>
                          <View style={styles.main}>
                            <View
                              style={{
                                position: 'absolute',
                                bottom: 10,
                                right: 10,
                              }}>
                              <Image
                                source={require('../../assets/images/right-tik.png')}
                              />
                            </View>
                          </View>
                        </ImageBackground>
                      ) : (
                        <Image
                          source={{uri: item.node.image.uri}}
                          style={{width: '100%', height: 150, borderRadius: 16}}
                        />
                      )}
                    </View>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(10, 40, 80, 0.7)',
    padding: 30,
    paddingBottom: 10,
    borderRadius: 16,
  },
});

export default ExploreScreen;
