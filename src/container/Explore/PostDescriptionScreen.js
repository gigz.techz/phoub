import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import PostScreenHeader from './PostScreenHeader';
import {useNavigation} from '@react-navigation/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/FontAwesome6';
import {useDispatch, useSelector} from 'react-redux';
import {savePostImages} from '../../Service/redux/reducer/dataSlice';
import {getImagePath, photoPost} from '../../Service/Api';

const PostDescriptionScreen = ({route}) => {
  const navigation = useNavigation();
  const carouselData = route?.params?.data;
  const {hashTag, mentionPeople, postImages} = useSelector(state => state.data);
  const dispatch = useDispatch();

  const [index, setIndex] = useState(0);
  const isCarousel = useRef(null);
  const [images, setImages] = useState([]);
  const [content, setContent] = useState('');
  const [imagePath, setImagePath] = useState([]);

  useEffect(() => {
    dispatch(savePostImages(carouselData));
    const carouselDataArray = Object.values(carouselData || postImages);
    setImages(carouselDataArray);
    handleImages(carouselDataArray);
  }, [postImages || carouselData]);

  const handleImages = item => {
    const data = item;
    let imageData = [];
    data &&
      data.forEach(value => {
        if (value && value.node && value.node.image) {
          imageData.push(value.node.image);
        }
      });
    imageData && handlePathImage(imageData);
  };

  const handlePathImage = async item => {
    const response = await getImagePath(item);
    setImagePath(response.path);
  };

  const renderItem = ({item}) => (
    <View
      style={{
        marginTop: 20,
        alignItems: 'center',
        borderRadius: 10,
      }}>
      <Image
        source={{uri: item.node.image.uri}}
        style={{height: 268, width: '100%', borderRadius: 16}}
      />
    </View>
  );

  const handlePost = async () => {
    const data = {
      content: content,
      mentions: mentionPeople,
      tags: hashTag,
      mediaList: imagePath,
    };
    const response = await photoPost(data);
    console.log('PHOTO POST RESPONSE', response);
    response && navigation.navigate('Home');
  };
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF', padding: 10}}>
      <View>
        <PostScreenHeader text="Post" onPress={handlePost} />
      </View>
      <ScrollView>
        <View style={{marginTop: 10}}>
          <Carousel
            ref={isCarousel}
            data={images}
            renderItem={renderItem}
            sliderWidth={400}
            itemWidth={300}
            onSnapToItem={currentIndex => setIndex(currentIndex)}
          />
          <View style={{marginTop: -20}}>
            <Pagination
              dotsLength={images.length}
              activeDotIndex={index}
              carouselRef={isCarousel}
              dotStyle={{
                width: 15,
                height: 10,
                borderRadius: 10,
                backgroundColor: 'rgb(116, 135, 160)',
              }}
            />
          </View>
        </View>
        <View style={{padding: 30, height: 168}}>
          <TextInput
            style={{
              fontSize: 16,
              fontWeight: '400',
              width: '100%',
            }}
            placeholder="Type your description..."
            placeholderTextColor="#9C9C9C"
            onChangeText={setContent}
          />
        </View>
        <View style={{padding: 15}}>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
              backgroundColor: '#FFFFFF',
              borderRadius: 8,
            }}>
            <View style={{flex: 0, flexDirection: 'row'}}>
              <View>
                <Icon name="hashtag" size={18} color="#183761" />
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                  Add hastags
                </Text>
              </View>
            </View>
            <View style={{justifyContent: 'center'}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('AddHashTagScreen')}>
                <MaterialIcons
                  name="arrow-forward-ios"
                  size={15}
                  color="#183761"
                />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
              backgroundColor: '#FFFFFF',
              borderRadius: 8,
              marginTop: 10,
            }}>
            <View style={{flex: 0, flexDirection: 'row'}}>
              <View>
                <Icon name="at" size={18} color="#183761" />
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                  Tag people/account
                </Text>
              </View>
            </View>
            <View style={{justifyContent: 'center'}}>
              <TouchableOpacity
                onPress={() => navigation.navigate('TagAccountScreen')}>
                <MaterialIcons
                  name="arrow-forward-ios"
                  size={15}
                  color="#183761"
                />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
              backgroundColor: '#FFFFFF',
              borderRadius: 8,
              marginTop: 10,
            }}>
            <View style={{flex: 0, flexDirection: 'row'}}>
              <View>
                <MaterialIcons name="settings" size={18} color="#183761" />
              </View>
              <View style={{marginLeft: 20}}>
                <Text
                  style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                  Post settings
                </Text>
              </View>
            </View>
            <View style={{justifyContent: 'center'}}>
              <TouchableOpacity>
                <MaterialIcons
                  name="arrow-forward-ios"
                  size={15}
                  color="#183761"
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default PostDescriptionScreen;
