import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native';
import React, {useState} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome6';
import {useDispatch} from 'react-redux';
import {saveHashTag} from '../../Service/redux/reducer/dataSlice';

const AddHashTagScreen = () => {
  const navigation = useNavigation();
  const [hashtag, setHashTag] = useState('');
  const dispatch = useDispatch();

  const handleHashTag = () => {
    dispatch(saveHashTag(hashtag));
  };

  return (
    <View style={{height: '100%', backgroundColor: '#FFFFFF'}}>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: '#fff',
          padding: 10,
        }}>
        <View style={{justifyContent: 'center'}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color='#183761' />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={{color: '#2E2E2E', fontSize: 20, fontWeight: '700'}}>
            Hashtags
          </Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('PostDescriptionScreen')}>
            <View
              style={{
                backgroundColor: '#183761',
                padding: 10,
                paddingLeft: 15,
                paddingRight: 15,
                borderRadius: 16,
              }}>
              <Text style={{fontSize: 12, fontWeight: '400', color: '#FFFFFF'}}>
                Done
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{padding: 10}}>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={[styles.searchContainer, {borderColor: '#9C9C9C'}]}>
            <TextInput
              style={styles.input}
              placeholder="Add a hashtag"
              onChangeText={setHashTag}
            />
            <TouchableOpacity onPress={handleHashTag}>
              <Image source={require('../../assets/images/addHash.png')} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{padding: 10}}>
        <View
          style={{flex: 0, flexWrap: 'wrap', flexDirection: 'row', gap: 10}}>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              backgroundColor: '#465F81',
              padding: 10,
              width: 120,
              justifyContent: 'center',
              borderRadius: 40,
            }}>
            <View>
              <Text style={{color: '#FFFFFF', fontSize: 14, fontWeight: '400'}}>
                photography
              </Text>
            </View>
            <TouchableWithoutFeedback>
              <View style={{justifyContent: 'center', marginLeft: 5}}>
                <Icon name="minus" size={10} color="#FFFFFF" />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              backgroundColor: '#465F81',
              padding: 10,
              width: 120,
              justifyContent: 'center',
              borderRadius: 40,
            }}>
            <View>
              <Text style={{color: '#FFFFFF', fontSize: 14, fontWeight: '400'}}>
                landscape
              </Text>
            </View>
            <TouchableWithoutFeedback>
              <View style={{justifyContent: 'center', marginLeft: 5}}>
                <Icon name="minus" size={10} color="#FFFFFF" />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              backgroundColor: '#465F81',
              padding: 5,
              width: 80,
              justifyContent: 'center',
              borderRadius: 40,
            }}>
            <View style={{justifyContent: 'center'}}>
              <Text style={{color: '#FFFFFF', fontSize: 14, fontWeight: '400'}}>
                photo
              </Text>
            </View>
            <TouchableWithoutFeedback>
              <View style={{justifyContent: 'center', marginLeft: 5}}>
                <Icon name="minus" size={10} color="#FFFFFF" />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              backgroundColor: '#465F81',
              padding: 10,
              width: 120,
              justifyContent: 'center',
              borderRadius: 40,
            }}>
            <View>
              <Text style={{color: '#FFFFFF', fontSize: 14, fontWeight: '400'}}>
                photography
              </Text>
            </View>
            <TouchableWithoutFeedback>
              <View style={{justifyContent: 'center', marginLeft: 5}}>
                <Icon name="minus" size={10} color="#FFFFFF" />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              backgroundColor: '#465F81',
              padding: 10,
              width: 120,
              justifyContent: 'center',
              borderRadius: 40,
            }}>
            <View>
              <Text style={{color: '#FFFFFF', fontSize: 14, fontWeight: '400'}}>
                landscape
              </Text>
            </View>
            <TouchableWithoutFeedback>
              <View style={{justifyContent: 'center', marginLeft: 5}}>
                <Icon name="minus" size={10} color="#FFFFFF" />
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 8,
    height: 52,
    paddingHorizontal: 10,
    width: '100%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
  },
});

export default AddHashTagScreen;
