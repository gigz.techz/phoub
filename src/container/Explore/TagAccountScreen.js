import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import React, {useState} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/Entypo';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';
import {saveMentionPeople} from '../../Service/redux/reducer/dataSlice';

const TagAccountScreen = () => {
  const navigation = useNavigation();
  const [mention, setMention] = useState();
  const dispatch = useDispatch();

  const handleMention = () => {
    dispatch(saveMentionPeople(parseInt(mention)));
  };
  return (
    <View style={{height: '100%', backgroundColor: '#FFFFFF'}}>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: '#fff',
          padding: 10,
        }}>
        <View style={{justifyContent: 'center'}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color='#183761' />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={{color: '#2E2E2E', fontSize: 20, fontWeight: '700'}}>
            Tag Account
          </Text>
        </View>
        <View style={{justifyContent: 'center'}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('PostDescriptionScreen')}>
            <View
              style={{
                backgroundColor: '#183761',
                padding: 10,
                paddingLeft: 15,
                paddingRight: 15,
                borderRadius: 16,
              }}>
              <Text style={{fontSize: 12, fontWeight: '400', color: '#FFFFFF'}}>
                Done
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{padding: 10}}>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={[styles.searchContainer, {borderColor: '#9C9C9C'}]}>
            <TextInput
              style={styles.input}
              placeholder="Search for account..."
              onChangeText={setMention}
            />
            <TouchableOpacity onPress={handleMention}>
              <Image
                source={require('../../assets/images/search.png')}
                style={[styles.icon, {tintColor: '#183761'}]}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          padding: 10,
          justifyContent: 'space-around',
        }}>
        <View>
          <View>
            <Image
              style={{height: 56, width: 56, borderRadius: 40}}
              source={require('../../assets/images/testImg1.png')}
            />
          </View>
          <View>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Markus
            </Text>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Brown
            </Text>
          </View>
        </View>
        <View>
          <View>
            <Image
              style={{height: 56, width: 56, borderRadius: 40}}
              source={require('../../assets/images/testImg2.png')}
            />
          </View>
          <View>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Sanjeev
            </Text>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              G
            </Text>
          </View>
        </View>
        <View>
          <View>
            <Image
              style={{height: 56, width: 56, borderRadius: 40}}
              source={require('../../assets/images/testImg3.png')}
            />
          </View>
          <View>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Chris
            </Text>
          </View>
        </View>
        <View>
          <View>
            <Image
              style={{height: 56, width: 56, borderRadius: 40}}
              source={require('../../assets/images/testImg4.png')}
            />
          </View>
          <View>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Melina
            </Text>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Brown
            </Text>
          </View>
        </View>
        <View>
          <View>
            <Image
              style={{height: 56, width: 56, borderRadius: 40}}
              source={require('../../assets/images/testImg1.png')}
            />
          </View>
          <View>
            <Text
              style={{
                color: '#2E2E2E',
                fontSize: 14,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Marry
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          borderBottomWidth: 1,
          borderBottomColor: '#D9D9D9',
          marginLeft: 15,
          marginRight: 15,
        }}
      />
      <View style={{padding: 10, marginTop: 15}}>
        <View>
          <Text style={{color: '#A3AFC0', fontSize: 16, fontWeight: '400'}}>
            Tagged account
          </Text>
        </View>
        <View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
            }}>
            <View>
              <TouchableOpacity>
                <View style={{flex: 0, flexDirection: 'row'}}>
                  <View style={{width: 38, height: 38}}>
                    <Image
                      style={{borderRadius: 100, height: '100%', width: '100%'}}
                      source={require('../../assets/images/back-display.png')}
                    />
                  </View>
                  <View style={{marginLeft: 20, justifyContent: 'center'}}>
                    <Text
                      style={{
                        color: '#2E2E2E',
                        fontSize: 14,
                        fontWeight: '700',
                      }}>
                      Maria Klaskov D.
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{justifyContent: 'center'}}>
              <TouchableOpacity>
                <Icon name="cross" size={16} color="#183761" />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
            }}>
            <View>
              <TouchableOpacity>
                <View style={{flex: 0, flexDirection: 'row'}}>
                  <View style={{width: 38, height: 38}}>
                    <Image
                      style={{borderRadius: 100, height: '100%', width: '100%'}}
                      source={require('../../assets/images/back-display.png')}
                    />
                  </View>
                  <View style={{marginLeft: 20, justifyContent: 'center'}}>
                    <Text
                      style={{
                        color: '#2E2E2E',
                        fontSize: 14,
                        fontWeight: '700',
                      }}>
                      Johnson James
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{justifyContent: 'center'}}>
              <TouchableOpacity>
                <Icon name="cross" size={16} color="#183761" />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
            }}>
            <View>
              <TouchableOpacity>
                <View style={{flex: 0, flexDirection: 'row'}}>
                  <View style={{width: 38, height: 38}}>
                    <Image
                      style={{borderRadius: 100, height: '100%', width: '100%'}}
                      source={require('../../assets/images/back-display.png')}
                    />
                  </View>
                  <View style={{marginLeft: 20, justifyContent: 'center'}}>
                    <Text
                      style={{
                        color: '#2E2E2E',
                        fontSize: 14,
                        fontWeight: '700',
                      }}>
                      Shubhomoy Photo...
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{justifyContent: 'center'}}>
              <TouchableOpacity>
                <Icon name="cross" size={16} color="#183761" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 8,
    height: 52,
    paddingHorizontal: 10,
    width: '100%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
    color: "black"
  },
});

export default TagAccountScreen;
