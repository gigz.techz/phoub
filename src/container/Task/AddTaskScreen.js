import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  TextInput,
  Modal,
  Image,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import {addTask, searchProfile} from '../../Service/Api';
import {Calendar} from 'react-native-calendars';
import moment from 'moment';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
// import {handleAllTask} from './CalendarScreen';

const AddTaskScreen = ({route}) => {
  const date = route?.params?.data;
  const [show, setShow] = useState(false);
  const [taskName, setTaskName] = useState('');
  const [taskDescription, setTaskDescription] = useState('');
  const [showCalender, setShowCalender] = useState(false);
  const [dueDate, setDueDate] = useState('');
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [time, setTime] = useState('09:30 AM');
  const navigation = useNavigation();
  const [searchItem, setSearchItem] = useState('');
  const [searchData, setSearchData] = useState({});
  const [selectedItems, setSelectedItems] = useState([]);
  const [invitedUser, setInvitedUser] = useState([]);
  const [chipCard, setChipCard] = useState();

  const handleAddTask = async () => {
    const newDate = date?.normalDate;
    const data = {
      title: taskName,
      dueDate: time
        ? convertToCombinedFormat(dueDate ? dueDate : date.normalDate, time)
        : date.normalDate + 'T09:30:00',
      description: taskDescription,
      participantIds: invitedUser,
    };
    console.log('DATATA', date?.normalDate);
    const response = await addTask(data);
    navigation.navigate('CalendarScreen', {
      data: {dateString: date.normalDate},
    });
  };

  const convertToCombinedFormat = (dateString, timeString) => {
    const combinedDateTime = moment(
      `${dateString} ${timeString}`,
      'YYYY-MM-DD h:mm A',
    );
    const isoString = combinedDateTime.toISOString();

    return isoString;
  };

  const formatDate = inputDate => {
    const formattedDate = moment(inputDate).format('ddd, DD MMM, YYYY');
    return formattedDate;
  };
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const extractTimeFromISOString = dateString => {
    const formattedTime = moment(dateString).format('hh:mm A');

    return formattedTime;
  };
  const handleConfirm = date => {
    console.log('A date has been picked: ', extractTimeFromISOString(date));
    setTime(extractTimeFromISOString(date));
    hideDatePicker();
  };

  const handleRemoveChip = index => {
    const newArray = [...selectedItems];
    newArray.splice(index, 1);
    setSelectedItems(newArray);
  };

  const handleSearch = async () => {
    const response = await searchProfile(searchItem);
    console.log('SEARCH', response.content);
    setSearchData(response);
  };

  const toggleItemSelection = (index, itemName, itemId) => {
    const selectedIndex = selectedItems.findIndex(
      item => item.index === index && item.name === itemName,
    );

    let newSelectedItems = [...selectedItems];
    let apiPayload = [];

    if (selectedIndex !== -1) {
      newSelectedItems = newSelectedItems.filter(
        item => !(item.index === index && item.name === itemName),
      );
    } else {
      newSelectedItems.push({index, name: itemName, id: itemId});
    }

    newSelectedItems.forEach(item => {
      apiPayload.push(item.id);
    });

    console.log('SELECTED ITEMS', newSelectedItems, apiPayload);
    setInvitedUser(apiPayload);
    setSelectedItems(newSelectedItems);
  };

  return (
    <View style={[styles.container, {position: 'relative', height: '100%'}]}>
      <View style={styles.header}>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Icon name="cross" size={24} />
            </TouchableOpacity>
          </View>
          <View>
            <Text style={{fontSize: 20, fontWeight: '700', color: '#2E2E2E'}}>
              Add new task
            </Text>
          </View>
          <View>{}</View>
        </View>
      </View>

      <ScrollView>
        <View style={{padding: 30}}>
          <View>
            <View style={styles.content}>
              <Text style={styles.textContent}>Task for</Text>
              <View style={{flex: 0, flexDirection: 'row'}}>
                <Text style={styles.dayText}>
                  {dueDate ? formatDate(dueDate) : date.formatDate}
                </Text>
                <Text style={styles.timeText}>{time}</Text>
              </View>
            </View>
            <View
              style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
              <TouchableOpacity
                style={styles.editButton}
                onPress={() => setShowCalender(true)}>
                <Text style={styles.editButtonText}>Edit Date</Text>
              </TouchableOpacity>
              <View>
                <TouchableOpacity
                  style={[styles.editButton, {marginLeft: 10}]}
                  onPress={showDatePicker}>
                  <Text style={styles.editButtonText}>Edit Time</Text>
                </TouchableOpacity>
                <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  mode="time"
                  onConfirm={handleConfirm}
                  onCancel={hideDatePicker}
                />
              </View>
            </View>

            <View style={styles.blueButton}>
              <TextInput
                style={styles.blueButtonText}
                placeholder="Add Task Name"
                onChangeText={setTaskName}
              />
            </View>
            <View style={styles.descriptionBox}>
              <TextInput
                style={styles.descriptionText}
                placeholder="Describe the task..."
                onChangeText={setTaskDescription}
              />
            </View>
            <View style={{marginTop: 30}}>
              <View>
                <Text
                  style={{fontSize: 16, fontWeight: '600', color: '#2E2E2E'}}>
                  Search Member
                </Text>
              </View>
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 10,
                }}>
                <View
                  style={[styles.searchContainer, {borderColor: '#183761'}]}>
                  <TextInput
                    value={searchItem}
                    style={styles.input}
                    placeholder="Search..."
                    onChangeText={setSearchItem}
                  />
                  <TouchableOpacity onPress={() => handleSearch()}>
                    <Image
                      source={require('../../assets/images/search.png')}
                      style={[styles.icon, {tintColor: '#183761'}]}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                flexWrap: 'wrap',
                gap: 10,
                margin: 10,
              }}>
              {selectedItems &&
                selectedItems.map((item, i) => (
                  <View
                    style={{
                      backgroundColor: '#183761',
                      padding: 8,
                      borderRadius: 30,
                      flex: 0,
                      flexDirection: 'row',
                    }}>
                    <Text style={{color: '#FFFFFF'}}>{item.name}</Text>
                    <View style={{marginLeft: 5}}>
                      <TouchableOpacity onPress={() => handleRemoveChip(i)}>
                        <Icon name="cross" size={20} color="#D1D7DF" />
                      </TouchableOpacity>
                    </View>
                  </View>
                ))}
            </View>

            <View>
              {searchData &&
                searchData.content &&
                searchData.content.map((item, i) => {
                  const isSelected = selectedItems.some(
                    selected =>
                      selected.index === i && selected.name === item.name,
                  );

                  return (
                    <View key={i}>
                      <View
                        style={{
                          flex: 0,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          padding: 20,
                        }}>
                        <View style={{flex: 0, flexDirection: 'row'}}>
                          <View
                            style={{
                              height: 38,
                              width: 38,
                              alignSelf: 'center',
                            }}>
                            <Image
                              style={{
                                height: '100%',
                                width: '100%',
                                borderRadius: 100,
                              }}
                              source={require('../../assets/images/test1.png')}
                            />
                          </View>

                          <View style={{marginLeft: 20, alignSelf: 'center'}}>
                            <Text
                              style={{
                                fontSize: 14,
                                fontWeight: '700',
                                color: '#2E2E2E',
                              }}>
                              {item.name}
                            </Text>
                          </View>
                        </View>

                        <View style={{justifyContent: 'center'}}>
                          <TouchableOpacity
                            onPress={() => {
                              toggleItemSelection(i, item.name, item.id);
                              setChipCard(i);
                            }}>
                            <View
                              style={[
                                styles.dot,
                                isSelected ? null : {padding: 2},
                              ]}>
                              <View
                                style={{
                                  backgroundColor: isSelected
                                    ? '#0E213A'
                                    : '#FFFFFF',
                                  height: '100%',
                                  width: '100%',
                                  borderRadius: 100,
                                }}></View>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  );
                })}
            </View>

            <View style={[styles.additionalContainer, {marginBottom: 100}]}>
              <Text style={styles.additionalText}>Does not Repeat</Text>
              <View>
                <TouchableOpacity onPress={() => setShow(!show)}>
                  {show ? (
                    <View style={styles.buttonWrapperTwo}>
                      <View style={styles.nestedButton}></View>
                    </View>
                  ) : (
                    <View style={styles.buttonWrapperTwo1}>
                      <View style={styles.nestedButton1}></View>
                    </View>
                  )}
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>

      <View style={styles.container}>
        <Modal
          transparent={true}
          animationType="slide"
          visible={showCalender}
          onRequestClose={() => {
            setShowCalender(!showCalender);
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Calendar
                onDayPress={day => {
                  setDueDate(day.dateString);
                  setShowCalender(false);
                }}
              />
            </View>
          </View>
        </Modal>
      </View>
      <View
        style={[
          styles.bottomButtonContainer,
          {
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: '#FFFFFF',
          },
        ]}>
        <TouchableOpacity
          style={styles.nextButton}
          onPress={() => handleAddTask()}>
          <Text style={styles.nextButtonText}>Save task</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  header: {
    backgroundColor: 'white',
    width: '100%',
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    padding: 20,
    borderRadius: 10,
    shadowColor: '#888888',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 0.7,
    shadowRadius: 10,
    elevation: 5,
  },
  headerText: {
    color: '#183761',
    fontSize: 22,
    fontWeight: 'bold',
  },
  content: {},
  textContent: {
    fontSize: 16,
    color: '#707070',
    fontWeight: '400',
  },
  dayText: {
    fontSize: 30,
    color: '#2E2E2E',
    fontWeight: '600',
    marginTop: 10,
  },
  timeText: {
    fontSize: 16,
    color: '#2E2E2E',
    fontWeight: '600',
    marginLeft: 10,
    alignSelf: 'flex-end',
  },
  editButton: {
    backgroundColor: '#D8DFE3',
    borderRadius: 20,
    width: 120,
    alignSelf: 'center',
    justifyContent: 'center',
    padding: 10,
    marginTop: 10,
  },
  editButtonText: {
    color: '#183761',
    fontSize: 14,
    fontWeight: '400',
    textAlign: 'center',
  },

  blueButton: {
    borderRadius: 10,
    width: '100%',
    backgroundColor: 'white',
    padding: 10,
    shadowColor: '#7487A0',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 0.7,
    shadowRadius: 10,
    elevation: 5,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#7487A0',
    marginTop: 30,
  },
  blueButtonText: {
    color: '#7487A0',
    fontSize: 16,
    fontWeight: '400',
  },
  descriptionBox: {
    marginTop: 20,
    borderRadius: 10,
    height: 150,
    backgroundColor: 'white',
    padding: 10,
    shadowColor: '#183761',
    shadowOffset: {width: 5, height: 5},
    shadowOpacity: 0.7,
    shadowRadius: 10,
    elevation: 5,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#7487A0 ',
  },
  descriptionText: {
    fontSize: 14,
    color: '#7487A0',
    fontWeight: '400',
  },
  additionalContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 8,
    elevation: 5,
    marginTop: 20,
  },
  additionalText: {
    fontSize: 18,
    color: '#183761',
  },
  smallButton: {
    backgroundColor: '#183761',
    borderRadius: 5,
    height: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  smallButtonText: {
    color: '#183761',
    fontSize: 14,
  },
  bottomButtonContainer: {
    padding: 20,
  },
  nextButton: {
    backgroundColor: '#183761',
    borderRadius: 10,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  nextButtonText: {
    color: '#FFFFFF',
    fontSize: 20,
    fontWeight: '600',
  },
  nestedButton: {
    backgroundColor: 'white',
    borderRadius: 2,
    width: 25,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    transform: [{translateX: 12}, {translateY: 8}],
  },
  buttonWrapper: {
    alignItems: 'center',
    marginTop: 5,
    backgroundColor: 'black',
  },
  buttonWrapperTwo: {
    backgroundColor: '#183761',
    alignItems: 'center',
    borderRadius: 8,
    width: 65,
    height: 35,
  },
  nestedButton1: {
    backgroundColor: '#183761',
    borderRadius: 2,
    width: 25,
    height: 20,
    marginLeft: -5,
    transform: [{translateX: 12}, {translateY: 8}],
  },

  buttonWrapperTwo1: {
    backgroundColor: 'white',
    borderRadius: 8,
    width: 65,
    height: 35,
    borderWidth: 1,
    borderColor: '#EBEBEB',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 40,
    paddingHorizontal: 10,
    width: '100%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
  },
  flexBetween: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dateInput: {
    borderColor: '#9C9C9C',
    borderWidth: 1,
    borderRadius: 8,
    fontSize: 16,
    fontWeight: '400',
    padding: 10,
    paddingLeft: 35,
    paddingRight: 35,
  },

  dot: {
    backgroundColor: '#0E213A',
    height: 20,
    width: 20,
    borderRadius: 100,
  },
});
export default AddTaskScreen;
