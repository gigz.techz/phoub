import {View, Text, TouchableOpacity, Image, ScrollView} from 'react-native';
import React, {useEffect, useState} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Plus from 'react-native-vector-icons/FontAwesome6';
import DateCard from './DateCard';
import {useNavigation} from '@react-navigation/native';
import moment from 'moment';
import {getAllTask, getTaskByDate} from '../../Service/Api';

const CalendarScreen = ({navigation,route}) => {
  const selectDate = route?.params?.data;
  // const navigation = useNavigation();
  let displayedDates = [];
  const dateString = selectDate?.dateString;
  const dateParts = dateString?.split('-');
  const initialMonth = dateParts ? parseInt(dateParts[1], 10) : '';
  const initialYear = dateParts ? parseInt(dateParts[0], 10) : '';
  const [groupedTasks, setGroupedTasks] = useState({});

  const [calenderData, setCalenderData] = useState();
  const [currentMonth, setCurrentMonth] = useState(
    new Date(initialYear, initialMonth - 1),
  );

  useEffect(() => {
    setCurrentMonth(new Date(initialYear, initialMonth - 1));
    handleTaskByDate(new Date(initialYear, initialMonth - 1));
  }, [initialMonth, initialYear]);

  const handleBeforeClick = () => {
    handleTaskByDate(
      new Date(currentMonth.getFullYear(), currentMonth.getMonth() - 1),
    );
    setCurrentMonth(
      new Date(currentMonth.getFullYear(), currentMonth.getMonth() - 1),
    );
  };

  const handleAfterClick = () => {
    handleTaskByDate(
      new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1),
    );
    setCurrentMonth(
      new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1),
    );
  };

  const monthOptions = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  const handleTaskByDate = async item => {
    const data = item;
    const response = await getTaskByDate({
      month: data.getMonth(),
      year: currentMonth.getFullYear(),
    });
    setCalenderData(response);
  };

  const formatDate = inputDate => {
    const formattedDate = moment(inputDate).format('ddd, DD MMM, YYYY');
    return formattedDate;
  };

  const inputDate = selectDate?.dateString;
  const formattedDate = inputDate ? formatDate(inputDate): "";

  const data = {
    normalDate: selectDate?.dateString,
    formatDate: formattedDate,
  };

  useEffect(() => {
    const groupTasksByDueDate = () => {
      const groupedByDate = {};
      calenderData &&
        calenderData.tasks &&
        calenderData.tasks.forEach(task => {
          if (!groupedByDate[task.dueDate]) {
            groupedByDate[task.dueDate] = [];
          }
          groupedByDate[task.dueDate].push(task);
        });
      setGroupedTasks(groupedByDate);
    };
    groupTasksByDueDate();
  }, [calenderData, selectDate]);

  return (
    <View style={{padding: 10}}>
      <View
        style={{
          padding: 10,
          position: 'relative',
          height: '100%',
        }}>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <AntDesign name="arrowleft" size={24} color="#183761" />
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={{
                fontSize: 20,
                fontWeight: '700',
                color: '#0C1317',
                marginLeft: -20,
              }}>
              My calendar
            </Text>
          </View>
          <View>{}</View>
        </View>
        <View>
          <View style={{alignItems: 'center', marginTop: 10}}>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                padding: 10,
                backgroundColor: '#D8DFE3',
                borderRadius: 40,
                paddingLeft: 10,
                paddingRight: 10,
              }}>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => handleBeforeClick()}>
                  <Icon name="arrow-back-ios" size={15} color="#183761" />
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  color: '#183761',
                  fontSize: 14,
                  fontWeight: '400',
                  marginLeft: 10,
                  marginRight: 10,
                }}>
                {`${
                  monthOptions[currentMonth.getMonth()]
                } ${currentMonth.getFullYear()}`}
              </Text>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => handleAfterClick()}>
                  <Icon name="arrow-forward-ios" size={15} color="#183761" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <ScrollView>
            <View style={{marginTop: 20, marginBottom: 50}}>
              <DateCard groupedTasks={groupedTasks} />
            </View>
          </ScrollView>
        </View>
      </View>
      <View style={{position: 'absolute', bottom: 40, right: 20}}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('AddTaskScreen', {
              data: {...data},
            })
          }>
          <View
            style={{
              backgroundColor: '#183761',
              height: 65,
              width: 65,
              borderRadius: 16,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Plus name="plus" size={15} color="#FFFFFF" />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};
// export const handleAllTask = async () => {
//   const response = await getAllTask();
//   console.log('DATA', response);
//   calendarList = response;
// };
export default CalendarScreen;
