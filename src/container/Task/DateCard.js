import {View, Text, ScrollView} from 'react-native';
import React from 'react';
import moment from 'moment';

const DateCard = ({groupedTasks}) => {
  // const generateRandomLightColor = () => {
  //   const letters = '0123456789ABCDEF';
  //   let color = '#';
  //   for (let i = 0; i < 6; i++) {
  //     color += letters[Math.floor(Math.random() * 16)];
  //   }
  //   return color;
  // };

  const getRandomElementFromArray = arr => {
    const randomIndex = Math.floor(Math.random() * arr.length);
    return arr[randomIndex];
  };

  const myArray = [
    '#ef15cb',
    '#3e8e05',
    '#0b7c25',
    '#001a5e',
    '#8c07bc',
    '#014066',
    '#0e8c6e',
    '#82032d',
  ];

  const extractDayAndDate = inputDate => {
    const dateObj = moment(inputDate);
    const dayOfWeek = dateObj.format('ddd');
    const dateOfMonth = dateObj.format('D');

    return {dayOfWeek, dateOfMonth};
  };
  return (
    <View style={{}}>
      <ScrollView>
        {Object.keys(groupedTasks).map(value => (
          <>
            <View style={{flex: 0, flexDirection: 'row'}} key={value}>
              <View style={{width: '18%'}}>
                <View>
                  <Text
                    style={{
                      color: '#2E2E2E',
                      fontSize: 48,
                      fontWeight: '600',
                      textAlign: 'center',
                    }}>
                    {extractDayAndDate(value).dateOfMonth}
                  </Text>
                </View>
                <View>
                  <Text
                    style={{
                      color: '#707070',
                      fontSize: 24,
                      fontWeight: '400',
                      textAlign: 'center',
                    }}>
                    {extractDayAndDate(value).dayOfWeek}
                  </Text>
                </View>
              </View>
              <View style={{width: '95%', height: '100%'}}>
                {groupedTasks[value].map(task => (
                  <View
                    style={{
                      backgroundColor: getRandomElementFromArray(myArray),
                      borderRadius: 20,
                      width: '78%',
                      justifyContent: 'center',
                      marginLeft: 20,
                      height: 100,
                      marginTop: 5
                    }}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontSize: 16,
                        fontWeight: '700',
                        marginLeft: 30,
                        marginTop: 5,
                      }}>
                      {task.title}
                    </Text>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontSize: 14,
                        fontWeight: '600',
                        marginLeft: 30,
                        marginTop: 10,
                      }}>
                      {task.description}
                    </Text>
                    <ScrollView horizontal={true}>
                      <View
                        style={{
                          flex: 0,
                          flexDirection: 'row',
                          gap: 10,
                          paddingHorizontal: 10,
                        }}>
                        {task.participants.map((item, i) => (
                          <View>
                            <Text style={{color: '#fff'}}>{item.username}</Text>
                          </View>
                        ))}
                      </View>
                    </ScrollView>
                  </View>
                ))}
              </View>
            </View>
            <View
              style={{
                borderBottomColor: '#D8DFE3',
                borderBottomWidth: 1,
                width: '100%',
                marginTop: 10,
                marginBottom: 10,
              }}
            />
          </>
        ))}
      </ScrollView>
    </View>
  );
};

export default DateCard;
