import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  ToastAndroid,
} from 'react-native';
import React, {useState} from 'react';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/CustomButton';
import {ScrollView} from 'react-native';
import CommonStyle from '../../assets/styles/CommonStyle';

const FreelancerPersonalDetail = ({navigation}) => {
  const [fullName, setFullName] = useState('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [code, setCode] = useState('');
  const [mobile, setMobile] = useState('');

  const freelancerData = {
    fullName: fullName,
    address: address,
    city: city,
    code: code,
    mobileNumber: mobile,
    role: 'FREELANCER',
  };

  const handleValidate = () => {
    if (fullName && address && city && code && mobile) {
      navigation.navigate('FreelancerEmailDetail', {
        data: {...freelancerData},
      });
    } else
      ToastAndroid.showWithGravity(
        'Please Fill All fields',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
  };

  return (
    <View>
      <View style={styles.main}>
        <View>
          <ScrollView>
            <View>
              <View style={CommonStyle.LOADING_WRAPPER}>
                <View style={CommonStyle.LOADING_BOX} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_BOX} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_LINE_PENDING} />
                <View style={CommonStyle.LOADING_BOX_PENDING} />
                <View style={CommonStyle.LOADING_LINE_PENDING} />
                <View style={CommonStyle.LOADING_LINE_PENDING} />
                <View style={CommonStyle.LOADING_BOX_PENDING} />
              </View>
            </View>
            <View style={{marginTop: 100}}>
              <Text style={styles.head}>Personal Details</Text>
            </View>
            <View style={{marginTop: 10}}>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Full Name" onChange={setFullName} />
              </View>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Address" onChange={setAddress} />
              </View>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Your City" onChange={setCity} />
              </View>
              <View
                style={{
                  marginTop: 15,
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View style={{width: '15%'}}>
                  <TextInput
                    maxLength={2}
                    keyboardType="number-pad"
                    onChangeText={setCode}
                    style={styles.input}
                    placeholder="00"
                    placeholderTextColor="#FFFFFF"></TextInput>
                </View>
                <View style={{width: '80%'}}>
                  <TextInput
                    maxLength={10}
                    keyboardType="number-pad"
                    onChangeText={setMobile}
                    style={styles.input}
                    placeholder="Phone No."
                    placeholderTextColor="#FFFFFF"></TextInput>
                </View>
              </View>
              <View style={{marginTop: 25}}>
                <CustomButton onPress={() => handleValidate()} text="Next" />
              </View>
            </View>

            <View>
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 50,
                }}>
                <View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: '400',
                      color: '#FFFFFF',
                    }}>
                    Have an account already?
                  </Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                  <View>
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: '700',
                        color: '#FFFFFF',
                        marginLeft: 3,
                      }}>
                      Login
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 36,
    fontWeight: '600',
    lineHeight: 65,
  },
  backGroundImage: {
    height: '100%',
    width: '100%',
  },
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: '#183761',
    justifyContent: 'space-between',
    padding: 30,
    paddingBottom: 10,
  },
  input: {
    borderRadius: 8,
    borderWidth: 1,
    backgroundColor: 'rgba(52, 52, 52, 0.7)',
    padding: 15,
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: '400',
  },
});

export default FreelancerPersonalDetail;
