import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import React, {useState} from 'react';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/CustomButton';
import CustomPasswordInput from '../../components/CustomPasswordInput';
import {ScrollView} from 'react-native';
import CommonStyle from '../../assets/styles/CommonStyle';
import {studioLogin, studioRegistration} from '../../Service/Api';
import CustomServiceInput from '../../components/CustomServiceInput';
import {useSelector} from 'react-redux';

const FreelancerEmailDetail = ({navigation, route}) => {
  const freelancerData = route?.params?.data;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const {freelancerService} = useSelector(state => state.data);

  const freelancerRegister = async () => {
    const payload = {
      ...freelancerData,
      username: email,
      password: password,
      serviceList: freelancerService,
    };
    const response = await studioRegistration(payload);
    console.log('RESPONSE', response);
    ToastAndroid.showWithGravity(
      'SignUp Successfully',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );

    handleLogin(payload);
  };

  const handleValidate = () => {
    if (password && email) {
      freelancerRegister();
    } else
      ToastAndroid.showWithGravity(
        'Please Fill All fields',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
  };

  const handleLogin = async payload => {
    const result = await studioLogin(payload);
    if (result.token) {
      navigation.navigate('SuccessScreen');
    } else {
      ToastAndroid.showWithGravity(
        'Something went wrong',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      navigation.navigate('Classification');
    }
  };

  const checkPasswordStrength = password => {
    // Define your password strength criteria here
    const strongRegex = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})',
    );
    const mediumRegex = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})',
    );

    if (strongRegex.test(password)) {
      return 'Strong';
    } else if (mediumRegex.test(password)) {
      return 'Medium';
    } else {
      return 'Weak';
    }
  };

  return (
    <View>
      <View style={styles.main}>
        <View>
          <ScrollView>
            <View>
              <View style={CommonStyle.LOADING_WRAPPER}>
                <View style={CommonStyle.LOADING_BOX} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_BOX} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_BOX} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_LINE_PENDING} />
                <View style={CommonStyle.LOADING_BOX_PENDING} />
              </View>
            </View>
            <View style={{marginTop: 100}}>
              <Text style={styles.head}>Personal Details</Text>
            </View>
            <View style={{marginTop: 10}}>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Email address" onChange={setEmail} />
              </View>
              <View style={{marginTop: 15}}>
                <CustomPasswordInput
                  placeholder="Password (Minimum 8 digits)"
                  onChange={setPassword}
                />
                <View style={{marginTop: 10}}>
                  <View style={CommonStyle.PASSWORD_WRAPPER}>
                    {password == '' ? (
                      <>
                        <View style={CommonStyle.PASSWORD1}></View>
                        <View style={CommonStyle.PASSWORD_LABEL1}></View>
                      </>
                    ) : checkPasswordStrength(password) == 'Weak' ? (
                      <>
                        <View style={CommonStyle.PASSWORD4}></View>
                        <View style={CommonStyle.PASSWORD_LABEL4}></View>
                      </>
                    ) : checkPasswordStrength(password) == 'Medium' ? (
                      <>
                        <View style={CommonStyle.PASSWORD6}></View>
                        <View style={CommonStyle.PASSWORD_LABEL6}></View>
                      </>
                    ) : (
                      <>
                        <View style={CommonStyle.PASSWORD8}></View>
                        <View style={CommonStyle.PASSWORD_LABEL8}></View>
                      </>
                    )}
                    {/* <View style={CommonStyle.PASSWORD10}></View> */}
                    {/* <View style={CommonStyle.PASSWORD_LABEL8}></View> */}
                  </View>
                </View>
              </View>
              <View style={{marginTop: 15}}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: '600',
                    color: '#FFFFFF',
                  }}>
                  Add at least one service
                </Text>
              </View>
              <View style={{marginTop: 10}}>
                <CustomServiceInput
                  placeholder="Type of service you offer"
                  onPress={() => navigation.navigate('ServiceFreelancer')}
                />
              </View>

              <View style={{marginTop: 25}}>
                <CustomButton text="Create" onPress={() => handleValidate()} />
              </View>
            </View>
            <View>
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 50,
                }}>
                <View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: '400',
                      color: '#FFFFFF',
                    }}>
                    Have an account already?
                  </Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                  <View>
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: '700',
                        color: '#FFFFFF',
                        marginLeft: 3,
                      }}>
                      Login
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 36,
    fontWeight: '600',
    lineHeight: 65,
  },
  backGroundImage: {
    height: '100%',
    width: '100%',
  },
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: '#183761',
    padding: 30,
    paddingBottom: 10,
  },
});

export default FreelancerEmailDetail;
