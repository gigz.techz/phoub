import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  ToastAndroid,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/CustomButton';
import {Image} from 'react-native';
import {studioLogin} from '../../Service/Api';

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    const data = {
      username: email,
      password: password,
    };
    const response = await studioLogin(data);
    console.log('RESPONSE', response);
    if (response.token) {
      if (response.accountActive == true) {
        ToastAndroid.showWithGravity(
          'Success',
          ToastAndroid.SHORT,
          ToastAndroid.CENTER,
        );
        navigation.navigate('WelcomeSuccess');
      } else {
        navigation.navigate('CreatePassword', {
          data: {...data},
        });
      }
    } else {
      ToastAndroid.showWithGravity(
        'Incorrect Email or Password',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
    }
  };

  return (
    <View>
      <View style={styles.main}>
        <View style={{marginBottom: 100, alignItems: 'center'}}>
          <Image
            style={{borderRadius: 100}}
            source={require('../../assets/images/circleLogo.jpg')}
          />
        </View>
        <View>
          <View>
            <Text style={styles.head}>Login</Text>
          </View>
          <View style={{marginTop: 20}}>
            <CustomInput
              placeholder={'Mobile number/Email ID'}
              onChange={setEmail}
            />
          </View>
          <View style={{marginTop: 15}}>
            <CustomInput placeholder={'Password'} onChange={setPassword} />
          </View>
          <View style={{marginTop: 10, marginLeft: 10}}>
            <TouchableOpacity>
              <Text style={{fontSize: 16, fontWeight: '600', color: '#FFFFFF'}}>
                Forgot password?
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 30}}>
            <CustomButton text="Login" onPress={handleLogin} />
          </View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 15,
              marginBottom: 20,
            }}>
            <View>
              <Text style={{fontSize: 16, fontWeight: '400', color: '#FFFFFF'}}>
                Don’t have an account?
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => navigation.navigate('Classification')}>
              <View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: '700',
                    color: '#FFFFFF',
                    marginLeft: 5,
                  }}>
                  Create account
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 40,
    fontWeight: '500',
    lineHeight: 65,
  },
  main: {
    flex: 0,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    height: '100%',
    padding: 20,
    backgroundColor: '#183761',
  },
  signUp: {
    backgroundColor: '#FFFFFF',
    padding: 20,
    width: '100%',
    borderRadius: 8,
  },
  googleButton: {
    backgroundColor: '#223142',
    borderColor: '#FFFFFF',
    borderWidth: 1,
    width: '100%',
    padding: 20,
    marginTop: 10,
  },
});

export default LoginScreen;
