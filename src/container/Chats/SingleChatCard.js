import {View, Text, Image} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Entypo';
import {TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const SingleChatCard = ({title, subtext, pin, id}) => {
  const navigation = useNavigation();
  const data = {
    id: id,
    name: title,
  };

  return (
    <View
      style={{
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 20,
      }}>
      <View>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('SingleChatBox', {
              data: {...data},
            })
          }>
          <View style={{flex: 0, flexDirection: 'row'}}>
            <View style={{width: 38, height: 38}}>
              <Image
                style={{borderRadius: 100, height: '100%', width: '100%'}}
                source={require('../../assets/images/back-display.png')}
              />
            </View>
            <View style={{marginLeft: 20}}>
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                }}>
                <Text
                  style={{color: '#2E2E2E', fontSize: 14, fontWeight: '700'}}>
                  {title}
                </Text>
                {pin && (
                  <Image
                    style={{marginLeft: 20, right: 0}}
                    source={require('../../assets/images/pin.png')}
                  />
                )}
              </View>
              <View>
                <Text
                  style={{color: '#9C9C9C', fontSize: 12, fontWeight: '400'}}>
                  {subtext}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{justifyContent: 'center'}}>
        <TouchableOpacity>
          <Icon name="dots-three-vertical" size={16} color="#183761" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SingleChatCard;
