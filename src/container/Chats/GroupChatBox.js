import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useEffect, useLayoutEffect, useState} from 'react';
import ChatBoxHeader from './ChatBoxHeader';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import socketServices from '../../Utils/SocketService';
import {allGroupMessage, sendGroupMessage} from '../../Service/Api';
import {useSelector} from 'react-redux';

const GroupChatBox = ({route}) => {
  const groupData = route?.params?.data;
  console.log('groupId', groupData);
  const [message, setMessage] = useState('');
  const [allMessages, setAllMessages] = useState([]);
  const {user} = useSelector(state => state.data);

  useEffect(() => {
    const intervalId = setInterval(() => {
      handleAllMessage();
    }, 2000);

    return () => clearInterval(intervalId);
  }, [handleAllMessage]);

  const handleAllMessage = async () => {
    const response = await allGroupMessage(groupData?.id);
    console.log('ALL GROROROROROR', response);
    setAllMessages(response);
  };

  const handleSendMessage = async () => {
    const payload = {
      groupId: groupData?.id,
      message: message,
    };
    const response = await sendGroupMessage(payload);
    console.log('SEND MEE', response);
    setMessage('');
    handleAllMessage();
  };

  return (
    <View style={{height: '100%', backgroundColor: '#FFFFFF', padding: 10}}>
      <View>
        <ChatBoxHeader name={groupData?.groupName} />
      </View>
      <ScrollView style={{padding: 10}}>
        <View style={{marginBottom: 100}}>
          {allMessages &&
            allMessages.map(item =>
              user.userId == item.userId ? (
                <View style={{justifyContent: 'flex-end'}}>
                  <View>
                    <View style={{alignItems: 'flex-end'}}>
                      <Image
                        source={require('../../assets/images/back-display.png')}
                        style={{
                          width: 20,
                          height: 20,
                          borderRadius: 100,
                          marginRight: 9,
                          marginTop: 5,
                          justifyContent: 'flex-end',
                        }}
                      />
                    </View>
                    <View style={{alignItems: 'flex-end', marginTop: 10}}>
                      <View
                        style={{
                          height: 15,
                          width: 15,
                          borderRadius: 20,
                          backgroundColor: '#EF8322',
                          position: 'absolute',
                          top: -6,
                          right: 10,
                        }}></View>
                      <View
                        style={{
                          elevation: 8,
                          shadowColor: '#999',
                          padding: 12,
                          backgroundColor: '#EF8322',
                          borderRadius: 9,
                        }}>
                        <Text
                          style={{
                            fontSize: 13,
                            fontWeight: '400',
                            color: '#FFFFFF',
                          }}>
                          {item.message}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              ) : (
                <View style={{}}>
                  <Image
                    source={require('../../assets/images/back-display.png')}
                    style={{
                      width: 20,
                      height: 20,
                      borderRadius: 100,
                      marginRight: 12,
                      marginTop: 5,
                      marginLeft: 6,
                    }}
                  />

                  <View
                    style={{
                      alignItems: 'flex-start',
                      width: '85%',
                      marginTop: 10,
                      position: 'relative',
                    }}>
                    <View
                      style={{
                        height: 15,
                        width: 15,
                        borderRadius: 20,
                        backgroundColor: '#132C4E',
                        position: 'absolute',
                        top: -6,
                        left: 10,
                      }}></View>
                    <View
                      style={{
                        padding: 12,
                        backgroundColor: '#132C4E',
                        borderRadius: 9,
                      }}>
                      <Text
                        style={{
                          fontSize: 13,
                          fontWeight: '400',
                          color: '#FFFFFF',
                        }}>
                        {item.message}
                      </Text>
                    </View>
                  </View>
                </View>
              ),
            )}
        </View>
      </ScrollView>

      <View style={{padding: 10}}>
        <View style={[styles.searchContainer]}>
          <TextInput
            value={message}
            style={styles.input}
            placeholder="Type your message here...."
            placeholderTextColor="#D1D7DF"
            onChangeText={setMessage}
          />
          <TouchableOpacity onPress={() => handleSendMessage()}>
            <MaterialIcons name="send" size={20} color="#132C4E" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 40,
    height: 40,
    paddingHorizontal: 10,
    width: '100%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
    color: 'black',
  },
});

export default GroupChatBox;
