import { View, Text, Image, TouchableOpacity } from "react-native";
import React from "react";
import AntDesign from "react-native-vector-icons/AntDesign";
import Icon from "react-native-vector-icons/Entypo";
import { useNavigation } from "@react-navigation/native";

const ChatBoxHeader = ({name}) => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        flex: 0,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 10,
      }}
    >
      <View style={{ flex: 0, flexDirection: "row" }}>
        <View style={{ justifyContent: "center" }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color='#000000' />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: 38,
            height: 38,
            marginLeft: 20,
            justifyContent: "center",
          }}
        >
          <Image
            style={{ borderRadius: 100, height: "100%", width: "100%" }}
            source={require("../../assets/images/back-display.png")}
          />
        </View>
        <View style={{ marginLeft: 20 }}>
          <Text style={{ fontSize: 14, fontWeight: "700", color: "#2E2E2E" }}>
            {name}
          </Text>
          {/* <Text style={{ color: "#9C9C9C" }}>Last seen 19h ago</Text> */}
        </View>
      </View>
      <View
        style={{ justifyContent: "center", fontSize: 10, fontWeight: "400" }}
      >
        <TouchableOpacity>
          <Icon name="dots-three-vertical" size={16} color="#183761" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ChatBoxHeader;
