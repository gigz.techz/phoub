import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import {createGroup, searchProfile} from '../../Service/Api';

const CreateGroupChat = ({navigation}) => {
  const [show, setShow] = useState(false);
  const [searchItem, setSearchItem] = useState('');
  const [groupName, setGroupName] = useState('');
  const [searchData, setSearchData] = useState({});
  const [selectedItems, setSelectedItems] = useState([]);
  const [invitedUser, setInvitedUser] = useState([]);
  const [chipCard, setChipCard] = useState();

  const handleSearch = async () => {
    const response = await searchProfile(searchItem);
    console.log('SEARCH', response.content);
    setSearchData(response);
  };

  const toggleItemSelection = (index, itemName, itemId) => {
    const selectedIndex = selectedItems.findIndex(
      item => item.index === index && item.name === itemName,
    );

    let newSelectedItems = [...selectedItems];
    let apiPayload = [];

    if (selectedIndex !== -1) {
      newSelectedItems = newSelectedItems.filter(
        item => !(item.index === index && item.name === itemName),
      );
    } else {
      newSelectedItems.push({index, name: itemName, id: itemId});
    }

    newSelectedItems.forEach(item => {
      apiPayload.push(item.id);
    });

    console.log('SELECTED ITEMS', newSelectedItems);
    setInvitedUser(apiPayload);
    setSelectedItems(newSelectedItems);
  };

  const handleCreateGroup = async () => {
    const data = {
      invited: invitedUser,
      groupName: groupName,
    };
    const response = await createGroup(data);
    console.log('GROP RES', response);
    response && navigation.goBack();
  };

  const handleRemoveChip = index => {
    const newArray = [...selectedItems];
    newArray.splice(index, 1);
    setSelectedItems(newArray);
  };

  return (
    <View style={{padding: 10}}>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color='#183761' />
          </TouchableOpacity>
        </View>
        <View>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#0C1317',
            }}>
            Group
          </Text>
        </View>
        <View>
          <TouchableOpacity onPress={() => handleCreateGroup()}>
            <View
              style={{
                padding: 8,
                backgroundColor: '#183761',
                borderRadius: 30,
              }}>
              <Text style={{fontSize: 16, fontWeight: '600', color: '#FFFFFF'}}>
                Create
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{marginTop: 10}}>
        <View>
          <Text style={{fontSize: 14, fontWeight: '500', color: '#2E2E2E'}}>
            Group Name
          </Text>
        </View>
        <View style={{marginTop: 10}}>
          <View style={[styles.searchContainer, {borderColor: '#183761'}]}>
            <TextInput
              value={groupName}
              style={styles.input}
              placeholder="Enter Group Name"
              onChangeText={setGroupName}
            />
          </View>
        </View>
      </View>
      <View style={{marginTop: 20}}>
        <View>
          <Text style={{fontSize: 16, fontWeight: '600', color: '#2E2E2E'}}>
            Search Group Member
          </Text>
        </View>
        <View
          style={{
            flex: 0,
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 10,
          }}>
          <View style={[styles.searchContainer, {borderColor: '#183761'}]}>
            <TextInput
              value={searchItem}
              style={styles.input}
              placeholder="Search..."
              onChangeText={setSearchItem}
            />
            <TouchableOpacity onPress={() => handleSearch()}>
              <Image
                source={require('../../assets/images/search.png')}
                style={[styles.icon, {tintColor: '#183761'}]}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          flexWrap: 'wrap',
          gap: 10,
          margin: 10,
        }}>
        {selectedItems &&
          selectedItems.map((item, i) => (
            <View
              style={{
                backgroundColor: '#183761',
                padding: 8,
                borderRadius: 30,
                flex: 0,
                flexDirection: 'row',
              }}>
              <Text style={{color: '#FFFFFF'}}>{item.name}</Text>
              <View style={{marginLeft: 5}}>
                <TouchableOpacity onPress={() => handleRemoveChip(i)}>
                  <Entypo name="cross" size={20} color="#D1D7DF" />
                </TouchableOpacity>
              </View>
            </View>
          ))}
      </View>
      <ScrollView>
        <View>
          {searchData &&
            searchData.content &&
            searchData.content.map((item, i) => {
              const isSelected = selectedItems.some(
                selected => selected.index === i && selected.name === item.name,
              );

              return (
                <View key={i}>
                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      padding: 20,
                    }}>
                    <View style={{flex: 0, flexDirection: 'row'}}>
                      <View
                        style={{
                          height: 38,
                          width: 38,
                          alignSelf: 'center',
                        }}>
                        <Image
                          style={{
                            height: '100%',
                            width: '100%',
                            borderRadius: 100,
                          }}
                          source={require('../../assets/images/test1.png')}
                        />
                      </View>

                      <View style={{marginLeft: 20, alignSelf: 'center'}}>
                        <Text
                          style={{
                            fontSize: 14,
                            fontWeight: '700',
                            color: '#2E2E2E',
                          }}>
                          {item.name}
                        </Text>
                      </View>
                    </View>

                    <View style={{justifyContent: 'center'}}>
                      <TouchableOpacity
                        onPress={() => {
                          toggleItemSelection(i, item.name, item.id);
                          setChipCard(i);
                        }}>
                        <View
                          style={[
                            styles.dot,
                            isSelected ? null : {padding: 2},
                          ]}>
                          <View
                            style={{
                              backgroundColor: isSelected
                                ? '#0E213A'
                                : '#FFFFFF',
                              height: '100%',
                              width: '100%',
                              borderRadius: 100,
                            }}></View>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            })}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 40,
    paddingHorizontal: 10,
    width: '100%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
  },
  flexBetween: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dateInput: {
    borderColor: '#9C9C9C',
    borderWidth: 1,
    borderRadius: 8,
    fontSize: 16,
    fontWeight: '400',
    padding: 10,
    paddingLeft: 35,
    paddingRight: 35,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  dot: {
    backgroundColor: '#0E213A',
    height: 20,
    width: 20,
    borderRadius: 100,
  },
});

export default CreateGroupChat;
