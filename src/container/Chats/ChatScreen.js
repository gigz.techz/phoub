import {View, Text, StyleSheet, TextInput, Image, Modal} from 'react-native';
import React, {useEffect, useState} from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import ChatCard from './ChatCard';
import {TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native';
import {fetchALLChat, fetchALLGroup, searchProfile} from '../../Service/Api';
import SingleChatCard from './SingleChatCard';
import {useFocusEffect} from '@react-navigation/native';

const ChatScreen = ({navigation}) => {
  const [highlight, setHighlight] = useState(true);
  const [tab, setTab] = useState('allChats');
  const [allgroup, setAllGroup] = useState(null);
  const [allChat, setAllChats] = useState(null);
  const [showSearch, setShowSearch] = useState(false);
  const [searchData, setSearchData] = useState({});
  const [searchItem, setSearchItem] = useState('');

  useFocusEffect(
    React.useCallback(() => {
      const intervalId = setInterval(() => {
        handleAllGroup();
        handleAllSingleChat();
      }, 2000);

      return () => clearInterval(intervalId);
    }, [handleAllGroup, handleAllSingleChat]),
  );

  const handleAllGroup = async () => {
    const response = await fetchALLGroup();
    console.log('ALL GROUP', response.content);
    setAllGroup(response?.content);
  };

  const handleAllSingleChat = async () => {
    const response = await fetchALLChat();
    console.log('ALL CHATSS', response.content);
    setAllChats(response.content);
  };

  const handleSearch = async () => {
    setShowSearch(true);
    const response = await searchProfile(searchItem);
    console.log('SEARCH', response.content);
    setSearchData(response);
  };

  return (
    <View style={{padding: 10}}>
      <View
        onBlur={() => setHighlight(true)}
        style={{
          padding: 10,
          position: 'relative',
          height: '100%',
        }}>
        <View>
          {tab == 'allChats' ? (
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={[styles.searchContainer, {borderColor: '#183761'}]}>
                <TextInput
                  style={styles.input}
                  placeholder="Search..."
                  onChangeText={setSearchItem}
                  placeholderTextColor="#D1D7DF"
                />
                <TouchableOpacity onPress={() => handleSearch()}>
                  <Image
                    source={require('../../assets/images/search.png')}
                    style={[styles.icon, {tintColor: '#183761'}]}
                  />
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View style={[styles.searchContainer, {borderColor: '#183761'}]}>
                <TextInput
                  placeholderTextColor="black"
                  style={styles.input}
                  placeholder="Search Group"
                  onChangeText={setSearchItem}
                />
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/images/search.png')}
                    style={[styles.icon, {tintColor: '#183761'}]}
                  />
                </TouchableOpacity>
              </View>
            </View>
          )}

          <View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginTop: 10,
              }}>
              <TouchableWithoutFeedback onPress={() => setTab('allChats')}>
                <View
                  style={
                    tab == 'allChats' ? styles.selectTab : styles.unSelectTab
                  }>
                  <Text
                    style={
                      tab == 'allChats'
                        ? styles.selectTabText
                        : styles.unSelectTabText
                    }>
                    All Chats
                  </Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => setTab('group')}>
                <View
                  style={
                    tab == 'group' ? styles.selectTab : styles.unSelectTab
                  }>
                  <Text
                    style={
                      tab == 'group'
                        ? styles.selectTabText
                        : styles.unSelectTabText
                    }>
                    Group
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
            <ScrollView>
              <View>
                <ScrollView>
                  <View style={{marginBottom: 200}}>
                    {tab == 'allChats' ? (
                      <View style={{height: '100%'}}>
                        {/* <View>
                          <Text
                            style={{
                              fontSize: 16,
                              fontWeight: '400',
                              color: '#A3AFC0',
                              marginTop: 15,
                            }}>
                            Pinned Chats
                          </Text>
                        </View>
                        <View style={{marginTop: 10}}>
                          <ChatCard
                            title="Cindy Raymond Hooks"
                            subtext="Lorem ipsum dolor sit amet"
                          />
                        </View>
                        <View>
                          <ChatCard
                            title="Simon Michale (2)"
                            subtext="Lorem ipsum dolor sit amet"
                          />
                        </View> */}
                        <View>
                          <Text
                            style={{
                              fontSize: 16,
                              fontWeight: '400',
                              color: '#A3AFC0',
                              marginTop: 15,
                            }}>
                            All Chats
                          </Text>
                        </View>
                        {allChat &&
                          allChat.map(item => (
                            <View style={{marginTop: 10}}>
                              <SingleChatCard
                                title={item.fullName}
                                id={item.userId}
                              />
                            </View>
                          ))}

                        <View style={{alignItems: 'center', marginTop: 20}}>
                          <Text
                            style={{
                              color: '#A3AFC0',
                              fontSize: 14,
                              fontWeight: '400',
                            }}>
                            No more chats to show
                          </Text>
                        </View>
                      </View>
                    ) : (
                      <View style={{height: '100%'}}>
                        {/* <View>
                          <Text
                            style={{
                              fontSize: 16,
                              fontWeight: '400',
                              color: '#A3AFC0',
                              marginTop: 15,
                            }}>
                            Pinned Chats
                          </Text>
                        </View>
                        <View style={{marginTop: 10}}>
                          <ChatCard
                            title="International Studios"
                            subtext="Lorem ipsum dolor sit amet"
                          />
                        </View> */}

                        <View>
                          <Text
                            style={{
                              fontSize: 16,
                              fontWeight: '400',
                              color: '#A3AFC0',
                              marginTop: 15,
                            }}>
                            All Groups
                          </Text>
                        </View>
                        {allgroup &&
                          allgroup.map(item => (
                            <View style={{marginTop: 10}}>
                              <ChatCard
                                title={item.groupName}
                                id={item.groupId}
                              />
                            </View>
                          ))}

                        <View style={{alignItems: 'center', marginTop: 20}}>
                          <Text
                            style={{
                              color: '#A3AFC0',
                              fontSize: 14,
                              fontWeight: '400',
                            }}>
                            No more groups to show
                          </Text>
                        </View>
                      </View>
                    )}
                  </View>
                </ScrollView>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
      <View style={{position: 'absolute', bottom: 40, right: 20}}>
        {tab == 'group' && (
          <TouchableOpacity>
            <Image
              style={{borderRadius: 50}}
              source={require('../../assets/images/groupChat.png')}
            />
          </TouchableOpacity>
        )}
      </View>
      {tab == 'allChats' && (
        <View style={styles.container}>
          <Modal
            transparent={false}
            animationType="slide"
            visible={showSearch}
            onRequestClose={() => {
              setShowSearch(!showSearch);
            }}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <View
                  style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={[styles.searchContainer, {borderColor: '#183761'}]}>
                    <TextInput
                      value={searchItem}
                      style={styles.input}
                      placeholder="Search..."
                      onChangeText={setSearchItem}
                      placeholderTextColor="#D1D7DF"
                    />
                    <TouchableOpacity onPress={() => handleSearch()}>
                      <Image
                        source={require('../../assets/images/search.png')}
                        style={[styles.icon, {tintColor: '#183761'}]}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View>
                  {searchData &&
                    searchData.content &&
                    searchData.content.map((item, i) => (
                      <View key={i}>
                        <TouchableOpacity
                          onPress={() =>
                            navigation.navigate('SingleChatBox', {
                              data: {
                                id: item.id,
                                name: item.name,
                              },
                            })
                          }>
                          <View
                            style={{
                              flex: 0,
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              padding: 20,
                            }}>
                            <View style={{flex: 0, flexDirection: 'row'}}>
                              <View
                                style={{
                                  height: 38,
                                  width: 38,
                                  alignSelf: 'center',
                                }}>
                                <Image
                                  style={{
                                    height: '100%',
                                    width: '100%',
                                    borderRadius: 100,
                                  }}
                                  source={require('../../assets/images/test1.png')}
                                />
                              </View>

                              <View
                                style={{marginLeft: 20, alignSelf: 'center'}}>
                                <Text
                                  style={{
                                    fontSize: 14,
                                    fontWeight: '700',
                                    color: '#2E2E2E',
                                  }}>
                                  {item.name}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </TouchableOpacity>
                      </View>
                    ))}
                </View>
              </View>
            </View>
          </Modal>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 40,
    height: 40,
    paddingHorizontal: 10,
    width: '100%',
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 5,
  },
  input: {
    flex: 1,
    color: 'black',
  },
  selectTab: {
    padding: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#183761',
    width: '50%',
  },
  unSelectTab: {
    padding: 10,
    width: '50%',
  },
  selectTabText: {
    fontSize: 16,
    fontWeight: '600',
    color: '#183761',
    textAlign: 'center',
  },
  unSelectTabText: {
    fontSize: 16,
    fontWeight: '400',
    color: '#183761',
    textAlign: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    height: '100%',
  },
  modalView: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    height: '100%',
  },
});

export default ChatScreen;
