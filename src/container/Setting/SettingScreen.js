import {View, Text, TouchableOpacity, ScrollView, Linking} from 'react-native';
import React, {useEffect, useState} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {getProfile} from '../../Service/Api';

const SettingScreen = ({navigation}) => {
  const [profileData, setProfileData] = useState({});
  const link1 = 'https://github.com/about';

  useEffect(() => {
    handleProfile();
  }, []);

  const handleProfile = async () => {
    const response = await getProfile();
    console.log('Profile', response);
    setProfileData(response);
  };
  const handleLogOut = async () => {
    navigation.navigate('WelcomeScreen');

    // try {
    //   await AsyncStorage.removeItem('BearerToken');
    //   await AsyncStorage.removeItem('username');
    //   await AsyncStorage.removeItem('password');
    //   console.log('Authentication token removed successfully');
    //   navigation.navigate('Login');
    // } catch (error) {
    //   console.log("error")
    // }
  };

  const handleLink = () => {
    Linking.openURL(link1);
  };
  return (
    <View style={{padding: 20}}>
      <View
        style={{
          flex: 0,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <AntDesign name="arrowleft" size={24} color="#183761" />
          </TouchableOpacity>
        </View>
        <View>
          <Text
            style={{
              fontSize: 20,
              fontWeight: '700',
              color: '#0C1317',
              marginLeft: -20,
            }}>
            Settings
          </Text>
        </View>
        <View>{}</View>
      </View>
      <View>
        <ScrollView>
          <View style={{marginBottom: 100}}>
            <View
              style={{
                marginTop: 30,
                padding: 5,
              }}>
              <Text
                style={{
                  color: '#A3AFC0',
                  fontSize: 16,
                  fontWeight: '400',
                }}>
                Profile settings
              </Text>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
              }}>
              <View style={{flex: 0, flexDirection: 'row'}}>
                <View>
                  <Icon name="user-circle-o" size={20} color="#183761" />
                </View>
                <View style={{marginLeft: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                    Personal information
                  </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('EditProfile', {
                      data: {...profileData},
                    })
                  }>
                  <MaterialIcons
                    name="arrow-forward-ios"
                    size={15}
                    color="#183761"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                marginTop: 10,
              }}>
              <View style={{flex: 0, flexDirection: 'row'}}>
                <View>
                  <MaterialIcons name="password" size={20} color="#183761" />
                </View>
                <View style={{marginLeft: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                    Change password
                  </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('CreatePassword')}>
                  <MaterialIcons
                    name="arrow-forward-ios"
                    size={15}
                    color="#183761"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                marginTop: 10,
              }}>
              <TouchableOpacity onPress={() => handleLogOut()}>
                <View style={{flex: 0, flexDirection: 'row'}}>
                  <View>
                    <TouchableOpacity>
                      <MaterialIcons name="logout" size={20} color="#183761" />
                    </TouchableOpacity>
                  </View>
                  <View style={{marginLeft: 20}}>
                    <Text
                      style={{
                        fontSize: 16,
                        color: '#183761',
                        fontWeight: '400',
                      }}>
                      Log out
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                marginTop: 30,
                padding: 5,
              }}>
              <Text
                style={{
                  color: '#A3AFC0',
                  fontSize: 16,
                  fontWeight: '400',
                }}>
                Others
              </Text>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                marginTop: 10,
              }}>
              <View>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                    About Phoub
                  </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={handleLink}>
                  <MaterialIcons
                    name="arrow-outward"
                    size={20}
                    color="#183761"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                marginTop: 10,
              }}>
              <View>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                    Help & Support
                  </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={handleLink}>
                  <MaterialIcons
                    name="arrow-outward"
                    size={20}
                    color="#183761"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                marginTop: 10,
              }}>
              <View>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                    Terms & Conditions
                  </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={handleLink}>
                  <MaterialIcons
                    name="arrow-outward"
                    size={20}
                    color="#183761"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                marginTop: 10,
              }}>
              <View>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#183761', fontWeight: '400'}}>
                    Contact us
                  </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity onPress={handleLink}>
                  <MaterialIcons
                    name="arrow-outward"
                    size={20}
                    color="#183761"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 20,
                backgroundColor: '#FFFFFF',
                borderRadius: 8,
                marginTop: 30,
              }}>
              <View style={{flex: 0, flexDirection: 'row'}}>
                <View>
                  <MaterialIcons name="delete" size={20} color="#F8504C" />
                </View>
                <View style={{marginLeft: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#F8504C', fontWeight: '400'}}>
                    Change password
                  </Text>
                </View>
              </View>
              <View style={{justifyContent: 'center'}}>
                <TouchableOpacity>
                  <MaterialIcons
                    name="arrow-forward-ios"
                    size={15}
                    color="#F8504C"
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginTop: 50}}>
              <Text
                style={{
                  color: '#D1D7DF',
                  fontSize: 14,
                  fontWeight: '400',
                  textAlign: 'center',
                }}>
                Version 1.2.3
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default SettingScreen;
