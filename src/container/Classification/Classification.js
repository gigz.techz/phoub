import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import {ImageBackground} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CommonStyle from '../../assets/styles/CommonStyle';

const Classification = ({navigation}) => {
  const [selected, setSelected] = useState(null);
  const handleSignUp = item => {
    if (item == 'customer') {
      navigation.navigate('CustomerPersonalDetail');
    } else if (item == 'freelancer') {
      navigation.navigate('FreelancerPersonalDetail');
    } else if (item == 'studio') {
      navigation.navigate('StudioPersonalDetail');
    }
  };
  return (
    <View>
      <View style={styles.main}>
        <View>
          <View style={CommonStyle.LOADING_WRAPPER}>
            <View style={CommonStyle.LOADING_BOX} />
            {selected ? (
              <View style={CommonStyle.LOADING_LINE} />
            ) : (
              <View style={CommonStyle.LOADING_LINE_PENDING} />
            )}

            <View style={CommonStyle.LOADING_LINE_PENDING} />
            <View style={CommonStyle.LOADING_BOX_PENDING} />
            <View style={CommonStyle.LOADING_LINE_PENDING} />
            <View style={CommonStyle.LOADING_LINE_PENDING} />
            <View style={CommonStyle.LOADING_BOX_PENDING} />
            <View style={CommonStyle.LOADING_LINE_PENDING} />
            <View style={CommonStyle.LOADING_LINE_PENDING} />
            <View style={CommonStyle.LOADING_BOX_PENDING} />
          </View>
        </View>
        <View>
          <View>
            <Text style={styles.head}>Welcome</Text>
          </View>
          <View style={{marginTop: 15}}>
            <Text style={styles.subHead}>
              Who do you classify as? Select one.
            </Text>
          </View>
          <View style={{marginTop: 15}}>
            <TouchableOpacity onPress={() => setSelected('customer')}>
              <View
                style={[
                  styles.input,
                  selected == 'customer'
                    ? {backgroundColor: '#FFFFFF'}
                    : {backgroundColor: '#132C4E'},
                ]}>
                {selected == 'customer' ? (
                  <View style={[styles.dot, {padding: 6}]}>
                    <View
                      style={{
                        backgroundColor: '#FFFFFF',
                        height: '100%',
                        width: '100%',
                        borderRadius: 100,
                      }}></View>
                  </View>
                ) : (
                  <View style={styles.dot}></View>
                )}

                <View style={{marginLeft: 20, alignSelf: 'center'}}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: '400',
                      color: selected == 'customer' ? '#183761' : '#FFFFFF',
                    }}>
                    I am a Customer
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <View
              style={{
                flex: 0,
                flexDirection: 'row',
                marginTop: 10,
                justifyContent: 'space-between',
              }}>
              <View style={{width: '48%'}}>
                <TouchableOpacity onPress={() => setSelected('freelancer')}>
                  <View
                    style={[
                      styles.input,
                      selected == 'freelancer'
                        ? {backgroundColor: '#FFFFFF'}
                        : {backgroundColor: '#132C4E'},
                    ]}>
                    {selected == 'freelancer' ? (
                      <View style={[styles.dot, {padding: 6}]}>
                        <View
                          style={{
                            backgroundColor: '#FFFFFF',
                            height: '100%',
                            width: '100%',
                            borderRadius: 100,
                          }}></View>
                      </View>
                    ) : (
                      <View style={styles.dot}></View>
                    )}
                    <View style={{marginLeft: 20, alignSelf: 'center'}}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: '400',
                          color:
                            selected == 'freelancer' ? '#183761' : '#FFFFFF',
                        }}>
                        Freelancer
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{width: '48%'}}>
                <TouchableOpacity onPress={() => setSelected('studio')}>
                  <View
                    style={[
                      styles.input,
                      selected == 'studio'
                        ? {backgroundColor: '#FFFFFF'}
                        : {backgroundColor: '#132C4E'},
                    ]}
                    onPress={() => setSelected('studio')}>
                    {selected == 'studio' ? (
                      <View style={[styles.dot, {padding: 6}]}>
                        <View
                          style={{
                            backgroundColor: '#FFFFFF',
                            height: '100%',
                            width: '100%',
                            borderRadius: 100,
                          }}></View>
                      </View>
                    ) : (
                      <View style={styles.dot}></View>
                    )}
                    <View style={{marginLeft: 20, alignSelf: 'center'}}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: '400',
                          color: selected == 'studio' ? '#183761' : '#FFFFFF',
                        }}>
                        Studio
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{marginTop: 50, opacity: selected ? 1 : 0.5}}>
            <CustomButton
              onPress={() => handleSignUp(selected)}
              text="Next"
              disabled={selected ? false : true}
            />
          </View>
        </View>
        <View>
          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 50,
            }}>
            <View>
              <Text style={{fontSize: 16, fontWeight: '400', color: '#FFFFFF'}}>
                Have an account already?
              </Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: '700',
                    color: '#FFFFFF',
                    marginLeft: 5,
                  }}>
                  Login
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  backGroundImage: {
    height: '100%',
    width: '100%',
  },
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: '#183761',
    justifyContent: 'space-between',
    padding: 30,
  },
  head: {
    color: '#FEFEFE',
    fontSize: 40,
    fontWeight: '600',
    lineHeight: 65,
    // textAlign: "center",
  },
  subHead: {
    fontSize: 16,
    fontWeight: '400',
    color: '#FFFFFF',
  },
  input: {
    padding: 20,
    borderRadius: 80,
    flex: 0,
    flexDirection: 'row',
    width: '100%',
  },
  dot: {
    backgroundColor: '#0E213A',
    height: 25,
    width: 25,
    borderRadius: 100,
  },
});

export default Classification;
