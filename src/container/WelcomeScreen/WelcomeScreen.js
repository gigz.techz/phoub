import {View, Text} from 'react-native';
import React from 'react';
import {ImageBackground} from 'react-native';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import CustomButton from '../../components/CustomButton';
import {Image} from 'react-native';

const WelcomeScreen = ({navigation}) => {
  return (
    <View>
      <View style={styles.main}>
        <View style={{marginBottom: 100, alignItems: 'center'}}>
          <Image style={{borderRadius: 100}} source={require('../../assets/images/circleLogo.jpg')} />
        </View>
        <View>
          <Text style={styles.head}>Welcome to Phoub</Text>
        </View>
        <View style={{width: '100%', marginTop: 20}}>
          <TouchableOpacity>
            <CustomButton
              text="Sign Up"
              onPress={() => navigation.navigate('Classification')}
            />
          </TouchableOpacity>
          <View>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 16,
                fontWeight: '400',
                color: '#FFFFFF',
                marginTop: 10,
              }}>
              or
            </Text>
          </View>
          <TouchableOpacity
            onPress={() => {
              GoogleSignin.configure({
                androidClientId:
                  '307725453550-psqsknsanuq2t4jb261p9133kmah4qj9.apps.googleusercontent.com',
                // iosClientId: 'ADD_YOUR_iOS_CLIENT_ID_HERE',
              });
              GoogleSignin.hasPlayServices()
                .then(hasPlayService => {
                  if (hasPlayService) {
                    GoogleSignin.signIn()
                      .then(userInfo => {
                        console.log(JSON.stringify(userInfo));
                      })
                      .catch(e => {
                        // console.log('ERROR IS: ' + JSON.stringify(e));
                      });
                  }
                })
                .catch(e => {
                  // console.log('ERROR IS: ' + JSON.stringify(e));
                });
            }}>
            <View style={styles.googleLogin}>
              <Image source={require('../../assets/images/googleLogo.png')} />
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '500',
                  color: '#D1D7DF',
                  textAlign: 'center',
                  marginLeft: 20,
                }}>
                Continue with Google
              </Text>
            </View>
          </TouchableOpacity>

          <View
            style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 15,
              marginBottom: 20,
            }}>
            <View>
              <Text style={{fontSize: 16, fontWeight: '400', color: '#FFFFFF'}}>
                Have an account already?
              </Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <View>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: '700',
                    color: '#FFFFFF',
                    marginLeft: 5,
                  }}>
                  Login
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 40,
    fontWeight: '500',
    lineHeight: 65,
  },
  main: {
    flex: 0,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    height: '100%',
    padding: 30,
    backgroundColor: '#183761',
  },

  googleLogin: {
    backgroundColor: '#223142',
    borderColor: '#FFFFFF',
    borderWidth: 1,
    width: '100%',
    padding: 20,
    marginTop: 10,
    borderRadius: 5,
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

export default WelcomeScreen;
