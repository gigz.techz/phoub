import {View, Text, Image} from 'react-native';
import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {studioLogin} from '../../Service/Api';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    checkLoginStatus();

    // setTimeout(() => {}, 1500);
  }, []);
  useEffect(() => {}, []);

  const checkLoginStatus = async () => {
    try {
      const authToken = await AsyncStorage.getItem('BearerToken');
      const username = await AsyncStorage.getItem('username');
      const password = await AsyncStorage.getItem('password');
      console.log('USERNAME PASSWORD', username, password);

      if (authToken) {
        const payload = {
          username: username,
          password: password,
        };
        const response = await studioLogin(payload);
        if (response.user_description == 'An error occurred') {
          navigation.navigate('WelcomeScreen');
        } else {
          navigation.navigate('Parent');
        }
      } else {
        navigation.navigate('WelcomeScreen');
      }
    } catch (error) {
      navigation.navigate('Login');
    }
  };
  return (
    <View
      style={{
        backgroundColor: '#183761',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View>
        <Image source={require('../../assets/images/logo.png')} />
      </View>
    </View>
  );
};

export default SplashScreen;
