import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Alert,
  ToastAndroid,
} from 'react-native';
import React, {useState} from 'react';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/CustomButton';
import CustomPasswordInput from '../../components/CustomPasswordInput';
import CommonStyle from '../../assets/styles/CommonStyle';
import {ScrollView} from 'react-native';
import {
  changePassword,
  studioLogin,
  studioRegistration,
} from '../../Service/Api';

const CreatePassword = ({navigation, route}) => {
  const customerData = route?.params?.data;
  const [password, setPassword] = useState('');
  const [confirmpass, setConfirmPass] = useState('');

  const customerRegister = async () => {
    if (password == confirmpass) {
      const payload = {
        currentPassword: customerData.password,
        password: password,
        username: customerData.username,
      };
      const response = await changePassword(payload);
      console.log('RESPONSE', response);
      ToastAndroid.showWithGravity(
        'Success',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );

      handleLogin(payload);
    } else {
      ToastAndroid.showWithGravity(
        'Match the password',
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
    }
  };

  const handleLogin = async payload => {
    const result = await studioLogin(payload);
    result && navigation.navigate('SuccessScreen');
  };

  const checkPasswordStrength = password => {
    // Define your password strength criteria here
    const strongRegex = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})',
    );
    const mediumRegex = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,})',
    );

    if (strongRegex.test(password)) {
      return 'Strong';
    } else if (mediumRegex.test(password)) {
      return 'Medium';
    } else {
      return 'Weak';
    }
  };

  return (
    <View>
      <View style={styles.main}>
        <ScrollView>
          <View>
            <View style={CommonStyle.LOADING_WRAPPER}>
              <View style={CommonStyle.LOADING_BOX} />
              <View style={CommonStyle.LOADING_LINE} />
              <View style={CommonStyle.LOADING_LINE} />
              <View style={CommonStyle.LOADING_BOX} />
              <View style={CommonStyle.LOADING_LINE} />
              <View style={CommonStyle.LOADING_LINE} />
              <View style={CommonStyle.LOADING_BOX} />
              <View style={CommonStyle.LOADING_LINE} />
              <View style={CommonStyle.LOADING_LINE_PENDING} />
              <View style={CommonStyle.LOADING_BOX_PENDING} />
            </View>
          </View>
          <View style={{marginTop: 100}}>
            <Text style={styles.head}>Password</Text>
          </View>
          <View style={{marginTop: 15}}>
            <Text style={{fontSize: 16, fontWeight: '400', color: '#FFFFFF'}}>
              Set a strong 8-digit password
            </Text>
          </View>
          <View>
            <View style={{marginTop: 15}}>
              <CustomPasswordInput
                onChange={setPassword}
                placeholder="Set new password"
              />
            </View>
            <View style={{marginTop: 10}}>
              <View style={CommonStyle.PASSWORD_WRAPPER}>
                {password == '' ? (
                  <>
                    <View style={CommonStyle.PASSWORD1}></View>
                    <View style={CommonStyle.PASSWORD_LABEL1}></View>
                  </>
                ) : checkPasswordStrength(password) == 'Weak' ? (
                  <>
                    <View style={CommonStyle.PASSWORD4}></View>
                    <View style={CommonStyle.PASSWORD_LABEL4}></View>
                  </>
                ) : checkPasswordStrength(password) == 'Medium' ? (
                  <>
                    <View style={CommonStyle.PASSWORD6}></View>
                    <View style={CommonStyle.PASSWORD_LABEL6}></View>
                  </>
                ) : (
                  <>
                    <View style={CommonStyle.PASSWORD8}></View>
                    <View style={CommonStyle.PASSWORD_LABEL8}></View>
                  </>
                )}
                {/* <View style={CommonStyle.PASSWORD10}></View> */}
                {/* <View style={CommonStyle.PASSWORD_LABEL8}></View> */}
              </View>
            </View>
            <View style={{marginTop: 15}}>
              <CustomInput
                onChange={setConfirmPass}
                placeholder="Confirm password"
                password={true}
              />
            </View>

            <View style={{marginTop: 25}}>
              <CustomButton
                text="Set password"
                onPress={() => customerRegister()}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 40,
    fontWeight: '600',
    lineHeight: 65,
  },
  backGroundImage: {
    height: '100%',
    width: '100%',
  },
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: '#183761',
    padding: 30,
    paddingBottom: 10,
  },
});

export default CreatePassword;
