import {View, Text} from 'react-native';
import React from 'react';
import {ImageBackground} from 'react-native';
import {StyleSheet} from 'react-native';
import {Image} from 'react-native';
import CustomButton from '../../components/CustomButton';
import CommonStyle from '../../assets/styles/CommonStyle';
import {studioRegistration} from '../../Service/Api';

const EmailVerification = ({route, navigation}) => {
  const customerData = route?.params?.data;

  const customerRegister = async () => {
    const payload = {
      ...customerData,
    };
    const response = await studioRegistration(payload);
    console.log('RESPONSE', response);
    response && navigation.navigate('Login');
  };

  return (
    <View>
      <View style={styles.main}>
        <View>
          <View style={CommonStyle.LOADING_WRAPPER}>
            <View style={CommonStyle.LOADING_BOX} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_BOX} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_LINE} />
            <View style={CommonStyle.LOADING_BOX} />
            <View style={CommonStyle.LOADING_LINE_PENDING} />
            <View style={CommonStyle.LOADING_LINE_PENDING} />
            <View style={CommonStyle.LOADING_BOX_PENDING} />
          </View>
        </View>
        <View style={{marginTop: 100}}>
          <View>
            <Text style={styles.head}>Verification</Text>
          </View>
          <View style={{marginTop: 5}}>
            <Text style={{fontSize: 16, fontWeight: '400', color: '#FFFFFF'}}>
              Password is sent to your inbox.
            </Text>
          </View>
          <View style={{alignItems: 'center', marginTop: 30}}>
            <Image source={require('../../assets/images/email.png')} />
          </View>
          <View style={{marginTop: 50}}>
            <CustomButton
              onPress={
                () => customerRegister()
                // navigation.navigate('CreatePassword', {
                //   data: {...customerData},
                // })
              }
              text="Open mail inbox for password"
            />
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 40,
    fontWeight: '600',
    lineHeight: 65,
  },
  backGroundImage: {
    height: '100%',
    width: '100%',
  },
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: '#183761',
    padding: 30,
  },
});

export default EmailVerification;
