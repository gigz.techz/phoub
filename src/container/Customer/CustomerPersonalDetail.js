import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Image,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/CustomButton';
import {ScrollView} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import CommonStyle from '../../assets/styles/CommonStyle';
import {getImagePath} from '../../Service/Api';

const CustomerPersonalDetail = ({navigation}) => {
  const [image, setImage] = useState(null);
  const [fullName, setFullName] = useState('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [email, setEmail] = useState('');
  const [profileImage, setProfileImage] = useState('');

  const openDocumentFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      setImage(res);
    } catch (err) {
      if (DocumentPicker.isCancle(err)) {
      } else {
        throw err;
      }
    }
  };

  useEffect(() => {
    image && handleImage(image);
  }, [image]);

  const handleImage = async item => {
    const response = await getImagePath(item);
    setProfileImage(response.path);
  };

  const customerData = {
    profileImage: profileImage,
    fullName: fullName,
    address: address,
    city: city,
    username: email,
    role: 'CUSTOMER',
  };

  return (
    <View>
      <View style={styles.main}>
        <View>
          <ScrollView>
            <View>
              <View style={CommonStyle.LOADING_WRAPPER}>
                <View style={CommonStyle.LOADING_BOX} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_BOX} />
                <View style={CommonStyle.LOADING_LINE} />
                <View style={CommonStyle.LOADING_LINE_PENDING} />
                <View style={CommonStyle.LOADING_BOX_PENDING} />
                <View style={CommonStyle.LOADING_LINE_PENDING} />
                <View style={CommonStyle.LOADING_LINE_PENDING} />
                <View style={CommonStyle.LOADING_BOX_PENDING} />
              </View>
            </View>
            <View style={{marginTop: 30}}>
              <Text style={styles.head}>Personal Details</Text>
            </View>
            <View style={{marginTop: 10}}>
              <View style={{alignSelf: 'center'}}>
                <View
                  style={{
                    backgroundColor: '#e3e3e3',
                    borderRadius: 100,
                    height: 108,
                    width: 108,
                  }}>
                  {image ? (
                    image.map((item, index) => (
                      <View key={index}>
                        <Image
                          style={{
                            resizeMode: 'contain',
                            height: 108,
                            width: 108,
                            borderRadius: 100,
                          }}
                          source={{uri: item.uri}}></Image>
                      </View>
                    ))
                  ) : (
                    <Image
                      style={{
                        borderRadius: 100,
                        height: '100%',
                        width: '100%',
                        alignSelf: 'center',
                      }}
                      source={require('../../assets/images/profile.png')}></Image>
                  )}
                </View>
                <View style={{alignSelf: 'center', marginTop: -15}}>
                  <TouchableOpacity onPress={() => openDocumentFile()}>
                    <View
                      style={{
                        backgroundColor: '#FFFFFF',
                        height: 30,
                        width: 30,
                        borderRadius: 100,
                      }}>
                      <Image
                        style={{
                          justifyContent: 'center',
                          height: '100%',
                          width: '100%',
                        }}
                        source={require('../../assets/images/upload.png')}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Full Name" onChange={setFullName} />
              </View>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Address" onChange={setAddress} />
              </View>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Your City" onChange={setCity} />
              </View>
              <View style={{marginTop: 15}}>
                <CustomInput placeholder="Email Address" onChange={setEmail} />
              </View>
              <View style={{marginTop: 25}}>
                <CustomButton
                  onPress={() =>
                    navigation.navigate('EmailVerification', {
                      data: {...customerData},
                    })
                  }
                  text="Next"
                />
              </View>
            </View>

            <View>
              <View
                style={{
                  flex: 0,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginTop: 50,
                }}>
                <View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: '400',
                      color: '#FFFFFF',
                    }}>
                    Have an account already?
                  </Text>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                  <View>
                    <Text
                      style={{
                        fontSize: 16,
                        fontWeight: '700',
                        color: '#FFFFFF',
                        marginLeft: 3,
                      }}>
                      Login
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  head: {
    color: '#FEFEFE',
    fontSize: 36,
    fontWeight: '600',
    lineHeight: 65,
  },
  backGroundImage: {
    height: '100%',
    width: '100%',
  },
  main: {
    height: '100%',
    width: '100%',
    backgroundColor: '#183761',
    padding: 30,
    justifyContent: 'space-between',
    paddingBottom: 10,
  },
});

export default CustomerPersonalDetail;
