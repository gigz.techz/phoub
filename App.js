import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import AppNavigator from './src/Utils/AppNavigator';

function App() {
  return <AppNavigator />;
}

const styles = StyleSheet.create({});

export default App;
